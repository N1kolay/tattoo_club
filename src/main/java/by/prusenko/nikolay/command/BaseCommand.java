package by.prusenko.nikolay.command;


import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;
import by.prusenko.nikolay.util.Router;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * The {@code BaseCommand} class
 * is an implementation of {@code SimpleCommand} interface,
 * is a super class for all commands
 * that have logic processing.
 *
 * @author Nikolay Prusenko
 */
public abstract class BaseCommand implements SimpleCommand {

    private ActionService actionService;

    public BaseCommand(ActionService actionService) {
        this.actionService = actionService;
    }

    /**
     * Returns a Map that has a {@code String} name as key and {@code String} parameter as value.
     * Each command should have own implementation.
     * <p>
     *
     * @param request an {@link HttpServletRequest} object that
     *                contains the request the client has made of the servlet
     * @return a Map contains {@code String} parameter as value.
     */
    protected abstract Map<String, String> retrieveRequestParameters(HttpServletRequest request);

    /**
     * Returns a list {@code String} of required parameters.
     * Each command should have own implementation.
     * <p>
     *
     * @return a List - contains names of parameters as {@code String}
     */
    protected abstract List<String> getRequiredParameters();

    /**
     * Sets the default router type to REDIRECT
     * <p>
     *
     * @return {@code Router.RouterType}.
     */
    protected Router.RouterType getRouterType() {
        return Router.RouterType.REDIRECT;
    }

    /**
     * Updates the request using the map of attributes
     * Each command should have own implementation.
     * <p>
     *
     * @param request    an {@link HttpServletRequest} object that
     *                   contains the request the client has made of the servlet
     * @param attributes -  map that has a {@code String} name of parameter as key
     *                   and {@code String} parameter as value.
     */
    protected void updateRequest(HttpServletRequest request, Map<String, String> attributes) {
    }

    /**
     * Updates the request using the map of attributes
     * Each command should have own implementation.
     * <p>
     *
     * @param request        an {@link HttpServletRequest} object that
     *                       contains the request the client has made of the servlet
     * @param listAttributes -  map that has a {@code String} name of parameter as key
     *                       and {@code List} parameter as value.
     */
    protected void updateRequestList(HttpServletRequest request, Map<String, List> listAttributes) {
    }

    /**
     * Sets message to the request using the map of messages.
     * Each command should have own implementation.
     * <p>
     *
     * @param request an {@link HttpServletRequest} object that
     *                contains the request the client has made of the servlet
     * @param message - map that has a {@code String} name of parameter as key
     *                and {@code String} parameter as value.
     */
    protected void sendMessageRequest(HttpServletRequest request, Map<String, String> message) {
    }

    /**
     * Validates parameters from request.
     * <p>
     *
     * @param parameters - map that has a {@code String} name of parameter as key
     *                   and {@code String} parameter as value.
     */
    private void validateRequestParameters(Map<String, String> parameters) {
        getRequiredParameters().forEach(requiredParameter -> {
            if (StringUtils.isBlank(parameters.get(requiredParameter)))
                throw new IllegalArgumentException("Parameter - " + requiredParameter + " is missing");
        });
    }

    /**
     * Executes request by invoking special methods.
     * Sets path and type to router and returns it.
     * <p>
     *
     * @param request an {@link HttpServletRequest} object that
     *                contains the request the client has made of the servlet
     * @return an {@code Router} object.
     */
    @Override
    public Router execute(HttpServletRequest request) {

        Map<String, String> requestParameters = retrieveRequestParameters(request);
        String path;
        validateRequestParameters(requestParameters);
        try {
            path = actionService.processAction(requestParameters);
        } catch (ServiceException e) {
            path = Page.MESSAGE + "?message=" + e.getMessage();
        }
        Map<String, String> attributes = actionService.receiveAttributes();
        Map<String, List> listAttributes = actionService.receiveListAttributes();
        Map<String, String> message = actionService.receiveMessage();
        if (attributes != null) {
            updateRequest(request, attributes);
        }
        if (listAttributes != null) {
            updateRequestList(request, listAttributes);
        }
        if (message != null) {
            sendMessageRequest(request, message);
        }
        return new Router(path, getRouterType());
    }
}
