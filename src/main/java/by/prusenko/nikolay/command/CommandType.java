package by.prusenko.nikolay.command;

import by.prusenko.nikolay.command.impl.base.*;
import by.prusenko.nikolay.command.impl.feedback.AddAnswerCommand;
import by.prusenko.nikolay.command.impl.feedback.AddFeedbackCommand;
import by.prusenko.nikolay.command.impl.feedback.DeleteFeedbackCommand;
import by.prusenko.nikolay.command.impl.feedback.FeedbackCommand;
import by.prusenko.nikolay.command.impl.news.*;
import by.prusenko.nikolay.command.impl.parlor.*;
import by.prusenko.nikolay.dao.FeedbackDao;
import by.prusenko.nikolay.dao.NewsDao;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.dao.UserDao;
import by.prusenko.nikolay.dao.impl.FeedbackDaoImpl;
import by.prusenko.nikolay.dao.impl.NewsDaoImpl;
import by.prusenko.nikolay.dao.impl.ParlorDaoImpl;
import by.prusenko.nikolay.dao.impl.UserDaoImpl;
import by.prusenko.nikolay.service.impl.base.ConfirmSignUpService;
import by.prusenko.nikolay.service.impl.base.LanguageService;
import by.prusenko.nikolay.service.impl.base.LoginService;
import by.prusenko.nikolay.service.impl.base.SignUpService;
import by.prusenko.nikolay.service.impl.feedback.AddAnswerService;
import by.prusenko.nikolay.service.impl.feedback.AddFeedbackService;
import by.prusenko.nikolay.service.impl.feedback.DeleteFeedbackService;
import by.prusenko.nikolay.service.impl.feedback.FeedbackService;
import by.prusenko.nikolay.service.impl.news.*;
import by.prusenko.nikolay.service.impl.parlor.*;

/**
 * The {@code CommandType} class
 * is enum of all commands.
 * Each type has an {@code SimpleCommand} object represented by this type.
 *
 * @author Nikolay Prusenko
 */
public enum CommandType {
    // Base
    LOGIN {
        {
            UserDao userDao = new UserDaoImpl();
            LoginService loginService = new LoginService(userDao);
            this.command = new LoginCommand(loginService);
        }
    },
    LOGOUT {
        {
            this.command = new LogoutCommand();
        }
    },
    SIGNUP {
        {
            UserDao userDao = new UserDaoImpl();
            SignUpService signUpService = new SignUpService(userDao);
            this.command = new SignUpCommand(signUpService);
        }
    },
    LANGUAGE{
        {
            LanguageService languageService = new LanguageService();
            this.command = new LanguageCommand(languageService);
        }
    },
    CONFIRMSIGNUP{
        {
            UserDao userDao = new UserDaoImpl();
            ConfirmSignUpService confirmSignUpService = new ConfirmSignUpService(userDao);
            this.command = new ConfirmSignUpCommand(confirmSignUpService);
        }
    },

    // Parlor
    ADDPARLOR {
        {
            ParlorDao parlorDao = new ParlorDaoImpl();
            AddParlorService addParlorService = new AddParlorService(parlorDao);
            this.command = new AddParlorCommand(addParlorService);
        }
    },
    INACTIVEPARLOR {
        {
            ParlorDao parlorDao = new ParlorDaoImpl();
            InactiveParlorService inactiveParlorService = new InactiveParlorService(parlorDao);
            this.command = new InactiveParlorCommand(inactiveParlorService);
        }
    },
    HANDLEINACTIVEPARLOR {
        {
            ParlorDao parlorDao = new ParlorDaoImpl();
            HandleInactiveParlorService handleInactiveParlorService = new HandleInactiveParlorService(parlorDao);
            this.command = new HandleInactiveParlorCommand(handleInactiveParlorService);
        }
    },
    DELETEPARLOR {
        {
            ParlorDao parlorDao = new ParlorDaoImpl();
            DeleteParlorService deleteParlorService = new DeleteParlorService(parlorDao);
            this.command = new DeleteParlorCommand(deleteParlorService);
        }
    },
    ALLPARLORS {
        {
            ParlorDao parlorDao = new ParlorDaoImpl();
            AllParlorsService allParlorsService = new AllParlorsService(parlorDao);
            this.command = new AllParlorCommand(allParlorsService);
        }
    },
    PARLORBYCITY {
        {
            ParlorDao parlorDao = new ParlorDaoImpl();
            ParlorByCityService parlorByCityService = new ParlorByCityService(parlorDao);
            this.command = new ParlorByCityCommand(parlorByCityService);
        }
    },

    // News
    ADDNEWS {
        {
            NewsDao newsDao = new NewsDaoImpl();
            AddNewsService addNewsServiceNewsService = new AddNewsService(newsDao);
            this.command = new AddNewsCommand(addNewsServiceNewsService);
        }
    },
    NEWS {
        {
            NewsDao newsDao = new NewsDaoImpl();
            NewsService newsService = new NewsService(newsDao);
            this.command = new NewsCommand(newsService);
        }
    },
    EDITALLNEWS {
        {
            NewsDao newsDao = new NewsDaoImpl();
            EditAllNewsService editAllNewsService = new EditAllNewsService(newsDao);
            this.command = new EditAllNewsCommand(editAllNewsService);
        }
    },
    INACTIVENEWS {
        {
            NewsDao newsDao = new NewsDaoImpl();
            InactiveNewsService inactiveNewsService = new InactiveNewsService(newsDao);
            this.command = new InactiveNewsCommand(inactiveNewsService);
        }
    },
    HANDLEINACTIVENEWS {
        {
            NewsDao newsDao = new NewsDaoImpl();
            HandleInactiveNewsService handleinactiveNewsService = new HandleInactiveNewsService(newsDao);
            this.command = new HandleInactiveNewsCommand(handleinactiveNewsService);
        }
    },

    // Feedback
    FEEDBACK {
        {
            FeedbackDao feedbackDao = new FeedbackDaoImpl();
            FeedbackService feedbackService = new FeedbackService(feedbackDao);
            this.command = new FeedbackCommand(feedbackService);
        }
    },
    ADDANSWER {
        {
            FeedbackDao feedbackDao = new FeedbackDaoImpl();
            AddAnswerService addAnswerService = new AddAnswerService(feedbackDao);
            this.command = new AddAnswerCommand(addAnswerService);
        }
    },

    ADDFEEDBACK {
        {
            FeedbackDao feedbackDao = new FeedbackDaoImpl();
            AddFeedbackService addFeedbackService = new AddFeedbackService(feedbackDao);
            this.command = new AddFeedbackCommand(addFeedbackService);
        }
    },
    DELETEFEEDBACK {
        {
            FeedbackDao feedbackDao = new FeedbackDaoImpl();
            DeleteFeedbackService deleteFeedbackService = new DeleteFeedbackService(feedbackDao);
            this.command = new DeleteFeedbackCommand(deleteFeedbackService);
        }
    },
   ;
    SimpleCommand command;
    public SimpleCommand getCurrentCommand() {
        return command;
    }
}
