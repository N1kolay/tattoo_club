package by.prusenko.nikolay.command;

import by.prusenko.nikolay.util.Router;

import javax.servlet.http.HttpServletRequest;
/**
 * The {@code SimpleCommand} interface
 * should be implemented by all commands.
 *
 * @author Nikolay Prusenko
 */
public interface SimpleCommand {
    /**
     * Executes the request.
     * Should have own implementation for each command.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return a {@code Router}.
     */
    Router execute(HttpServletRequest request);
}
