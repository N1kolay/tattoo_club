package by.prusenko.nikolay.command.factory;

import by.prusenko.nikolay.command.CommandType;
import by.prusenko.nikolay.command.SimpleCommand;
import by.prusenko.nikolay.command.impl.base.EmptyCommand;
import by.prusenko.nikolay.constant.Attribute;
import by.prusenko.nikolay.util.MessageManager;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * The {@code  ActionFactory} class
 * is a factory that defines and returns {@code SimpleCommand} object.
 *
 * @author Nikolay Prusenko
 */
public class ActionFactory {
    /**
     * Defines and returns {@code SimpleCommand} object.
     *
     * @param request is an {@link HttpServletRequest} object that
     * contains the type from which {@code SimpleCommand} object is defined.
     * @return {@code SimpleCommand} object.
     */
    public SimpleCommand defineCommand(HttpServletRequest request) {

        SimpleCommand current = new EmptyCommand();

        String action = request.getParameter("command");
        if (StringUtils.isBlank(action)) {
            return current;
        }
        try {
            CommandType currentEnum = CommandType.valueOf(action.toUpperCase());
            current = currentEnum.getCurrentCommand();
        } catch (IllegalArgumentException e) {
            request.setAttribute("wrongAction", action
                    + MessageManager.getProperty("message.wrongaction", Attribute.LOCALE));
        }
        return current;
    }
}
