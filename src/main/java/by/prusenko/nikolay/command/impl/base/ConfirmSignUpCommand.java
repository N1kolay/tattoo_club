package by.prusenko.nikolay.command.impl.base;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.base.ConfirmSignUpService;
import by.prusenko.nikolay.util.Router;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
/**
 * The {@code ConfirmSignUpCommand} class
 * is a command to confirm user registration.
 *
 * @author Nikolay Prusenko
 */
public class ConfirmSignUpCommand extends BaseCommand {
    private static final List<String>REQUIRED_PARAMETERS= Arrays.asList(ACTOR_NAME,TOKEN);
    public ConfirmSignUpCommand(ConfirmSignUpService confirmSignUpService) {
        super(confirmSignUpService);
    }
    /**
     * Gets name, token from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String name = request.getParameter(ACTOR_NAME);
        String token = request.getParameter(TOKEN);
        return Map.of(ACTOR_NAME,name,TOKEN,token);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }
    /**
     * Sets the router type to FORWARD
     *
     * @return {@code Router.RouterType}.
     */
    protected Router.RouterType getRouterType() {
        return Router.RouterType.FORWARD;
    }
}
