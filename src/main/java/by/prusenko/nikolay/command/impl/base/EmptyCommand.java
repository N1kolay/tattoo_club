package by.prusenko.nikolay.command.impl.base;

import by.prusenko.nikolay.command.SimpleCommand;
import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.util.Router;

import javax.servlet.http.HttpServletRequest;

/**
 * The {@code EmptyCommand} class
 * is a command to process empty command.
 *
 * @author Nikolay Prusenko
 */
public class EmptyCommand implements SimpleCommand {
    /**
     * Creates a new router object, sets the router type to REDIRECT
     * and sets its page.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return a {@code Router} object.
     */
    @Override
    public Router execute(HttpServletRequest request) {
        Router router=new Router();
        router.setType(Router.RouterType.REDIRECT);
        router.setPath(Page.MAIN);
        return router;
    }
}
