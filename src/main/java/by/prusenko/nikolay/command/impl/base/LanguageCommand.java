package by.prusenko.nikolay.command.impl.base;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.base.LanguageService;
import by.prusenko.nikolay.util.Router;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code LanguageCommand} class
 * is a command to set locale.
 *
 * @author Nikolay Prusenko
 */
public class LanguageCommand extends BaseCommand {
    private static final List<String> REQUIRED_PARAMETERS = Arrays.asList(LANG, PATH);
    public LanguageCommand(LanguageService languageService) {
        super(languageService);
    }

    /**
     * Gets locale value and path from the request and
     * returns Map with parameters.
     *
     * @param request an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     *
     * @return a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {

        String locale = request.getParameter(LANG);
        String path = (String) request.getSession().getAttribute(PATH);
        String servletPath = request.getServletPath();

        if (!path.startsWith("/")) {
            path = servletPath + "?" + path;
        }
        return Map.of(LANG, locale, PATH, path);
    }

    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }
    /**
     * Sets the router type to FORWARD
     *
     * @return {@code Router.RouterType}
     */
    @Override
    protected Router.RouterType getRouterType() {
        return Router.RouterType.FORWARD;
    }
    /**
     * Sets message to the request using the map of messages.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @param message - map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     */
    @Override
    protected void sendMessageRequest(HttpServletRequest request, Map<String, String> message) {
        request.getSession().setAttribute(LOCALE, message.get(LOCALE));
    }
}
