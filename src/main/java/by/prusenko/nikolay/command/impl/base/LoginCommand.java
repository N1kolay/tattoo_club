package by.prusenko.nikolay.command.impl.base;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.base.LoginService;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
/**
 * The {@code LoginCommand} class
 * is a command to log in (authorize) an actor.
 *
 * @author Nikolay Prusenko
 */
public class LoginCommand extends BaseCommand {
    private static final List<String> REQUIRED_PARAMETERS= Arrays.asList(ACTOR_EMAIL,ACTOR_PASSWORD);

    public LoginCommand(LoginService loginService) {
        super(loginService);
    }
    /**
     * Gets email, password from the request and
     * returns a map of parameters.
     *
     * @param request  an {@link HttpServletRequest} object that
     *                 contains the request the client has made of the servlet
     * @return a {@code Map} that has a {@code String} name of parameter as key
     *                      and {@code String} parameter as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String email = request.getParameter(ACTOR_EMAIL);
        String pass = request.getParameter(ACTOR_PASSWORD);
        return Map.of(ACTOR_EMAIL,email,ACTOR_PASSWORD,pass);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }
    /**
     * Updates the request using the map of attributes.
     *
     * @param request  an {@link HttpServletRequest} object that
     *                 contains the request the client has made of the servlet
     * @param loginAttributes -  map that has a {@code String} name of parameter as key
     *                        and {@code String} parameter as value.
     */
    @Override
    protected void updateRequest(HttpServletRequest request, Map<String,String> loginAttributes){
        HttpSession session = request.getSession(true);
        session.setAttribute(ACTOR_NAME, loginAttributes.get(ACTOR_NAME));
        session.setAttribute(ACTOR_ROLE, loginAttributes.get(ACTOR_ROLE));
        session.setAttribute(ID_PARLOR, loginAttributes.get(ID_PARLOR));
        session.setAttribute(ID_ACTOR, loginAttributes.get(ID_ACTOR));
    }
}

