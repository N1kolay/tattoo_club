package by.prusenko.nikolay.command.impl.base;

import by.prusenko.nikolay.command.SimpleCommand;
import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.util.Router;

import javax.servlet.http.HttpServletRequest;

/**
 * The {@code LogoutCommand} class
 * is a command to log out from system.
 *
 * @author Nikolay Prusenko
 */
public class LogoutCommand implements SimpleCommand {
    /**
     * Closes session, creates a new router object, sets its page
     * and sets the router type to REDIRECT
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return a {@code Router} object
     */
    @Override
    public Router execute(HttpServletRequest request) {
        request.getSession().invalidate();
        Router router = new Router();
        router.setType(Router.RouterType.REDIRECT);
        router.setPath(Page.MAIN);
        return router;
    }
}
