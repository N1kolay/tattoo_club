package by.prusenko.nikolay.command.impl.base;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.base.SignUpService;
import by.prusenko.nikolay.util.Router;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code  SignUpCommand} class
 * is a command to register new an actor.
 *
 * @author Nikolay Prusenko
 */
public class SignUpCommand extends BaseCommand {
    private static final List<String> REQUIRED_PARAMETERS = Arrays.asList(ACTOR_NAME,ACTOR_EMAIL,ACTOR_PASSWORD,ACTOR_PASSWORD_CONFIRMATION);

    public SignUpCommand(SignUpService signUpService) {
        super(signUpService);
    }
    /**
     * Gets name, email, password, confirmPassword from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String name = request.getParameter(ACTOR_NAME);
        String email = request.getParameter(ACTOR_EMAIL);
        String pass = request.getParameter(ACTOR_PASSWORD);
        String passConfirm = request.getParameter(ACTOR_PASSWORD_CONFIRMATION);
        return Map.of(ACTOR_NAME,name,ACTOR_EMAIL,email,ACTOR_PASSWORD,pass,ACTOR_PASSWORD_CONFIRMATION,passConfirm);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }
    /**
     * Sets the router type to FORWARD
     *
     * @return {@code Router.RouterType}.
     */
    @Override
    protected Router.RouterType getRouterType() {
        return Router.RouterType.FORWARD;
    }

    /**
     * Updates the request using the map of messages.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @param message -  map that has a {@code String} name of message as key and {@code String} message as value.
     */
    @Override
    protected void sendMessageRequest(HttpServletRequest request, Map<String, String> message) {
        request.setAttribute("message",message.get("message"));
    }
}
