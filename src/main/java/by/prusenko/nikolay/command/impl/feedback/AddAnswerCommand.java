package by.prusenko.nikolay.command.impl.feedback;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.feedback.AddAnswerService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code AddAnswerCommand} class
 * is a command to add answer to feedback.
 *
 * @author Nikolay Prusenko
 */
public class AddAnswerCommand extends BaseCommand {
    private static final List<String> REQUIRED_PARAMETERS = Arrays.asList(ANSWER, ID_FEEDBACK, ID_ACTOR, ID_PARLOR);

    public AddAnswerCommand(AddAnswerService addAnswerService) {
        super(addAnswerService);
    }

    /**
     * Gets answer, id of feedback, id of actor, id of parlor from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String answer = request.getParameter(ANSWER);
        String idFeedback = request.getParameter(ID_FEEDBACK);
        String idActor = (String) request.getSession().getAttribute(ID_ACTOR);
        String idParlor = (String) request.getSession().getAttribute(ID_PARLOR);
        return Map.of(ANSWER, answer, ID_FEEDBACK, idFeedback, ID_ACTOR, idActor, ID_PARLOR, idParlor);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }


}
