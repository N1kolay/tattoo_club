package by.prusenko.nikolay.command.impl.feedback;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.feedback.AddFeedbackService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code AddFeedbackCommand} class
 * is a command to add feedback to parlor.
 *
 * @author Nikolay Prusenko
 */
public class AddFeedbackCommand extends BaseCommand {
    private final static List<String> REQUIRED_PARAMETERS = Arrays.asList(ID_ACTOR, COMMENT, ID_PARLOR);

    public AddFeedbackCommand(AddFeedbackService addFeedbackService) {
        super(addFeedbackService);
    }
    /**
     * Gets id of actor, rating, comment, id of parlor from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String idActor = String.valueOf(request.getSession().getAttribute(ID_ACTOR));
        String rating = request.getParameter(RATING);
        String comment = request.getParameter(COMMENT);
        String idParlor = request.getParameter(ID_PARLOR);
        Map<String, String> parameters;
        if (rating == null) {
            parameters = Map.of(ID_ACTOR, idActor, RATING, "0", COMMENT, comment, ID_PARLOR, idParlor);
        } else {
            parameters = Map.of(ID_ACTOR, idActor, RATING, rating, COMMENT, comment, ID_PARLOR, idParlor);
        }
        return parameters;
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }


}
