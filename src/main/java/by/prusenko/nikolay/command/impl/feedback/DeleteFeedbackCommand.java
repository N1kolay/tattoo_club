package by.prusenko.nikolay.command.impl.feedback;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.feedback.DeleteFeedbackService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code DeleteFeedbackCommand} class
 * is a command to delete feedback.
 *
 * @author Nikolay Prusenko
 */
public class DeleteFeedbackCommand extends BaseCommand {
    private static final List<String> REQUIRED_PARAMETERS = Arrays.asList(ID_FEEDBACK, ID_PARLOR);

    public DeleteFeedbackCommand(DeleteFeedbackService deleteFeedbackService) {
        super(deleteFeedbackService);
    }
    /**
     * Gets id of feedback, answer, id of parlor from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String id = request.getParameter(ID_FEEDBACK);
        String answer = request.getParameter(ANSWER);
        String idParlor = request.getParameter(ID_PARLOR);
        Map<String, String> parameters;
        if (answer != null) {
            parameters = Map.of(ID_FEEDBACK, id, ANSWER, answer, ID_PARLOR, idParlor);
        } else {
            parameters = Map.of(ID_FEEDBACK, id, ID_PARLOR, idParlor);
        }

        return parameters;
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }

}
