package by.prusenko.nikolay.command.impl.feedback;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.feedback.FeedbackService;
import by.prusenko.nikolay.util.Router;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code FeedbackCommand} class
 * is a command to show all feedback of concrete parlor.
 *
 * @author Nikolay Prusenko
 */
public class FeedbackCommand extends BaseCommand {
    private static final List<String> REQUIRED_PARAMETERS = Collections.singletonList(ID_PARLOR);

    public FeedbackCommand(FeedbackService feedbackService) {
        super(feedbackService);
    }
    /**
     * Gets id of parlor from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String id = request.getParameter(ID_PARLOR);
        return Map.of(ID_PARLOR,id);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }
    /**
     * Sets the router type to FORWARD
     *
     * @return {@code Router.RouterType}
     */
    @Override
    protected Router.RouterType getRouterType() {
        return Router.RouterType.FORWARD;
    }
    /**
     * Updates the request using the map of attributes.
     *
     * @param request  an {@link HttpServletRequest} object that
     *                 contains the request the client has made of the servlet
     * @param attributes -  map that has a {@code String} name of parameter as key
     *                        and {@code String} parameter as value.
     */
    @Override
    protected void updateRequest(HttpServletRequest request, Map<String, String> attributes) {
        request.setAttribute(ID_PARLOR, attributes.get(ID_PARLOR));
    }
    /**
     * Updates the request using the map of attributes.
     *
     * @param request  an {@link HttpServletRequest} object that
     *                 contains the request the client has made of the servlet
     * @param feedbackList -  map that has a {@code String} name of parameter as key
     *                        and {@code List} parameter as value.
     */
    @Override
    protected void updateRequestList(HttpServletRequest request, Map<String, List> feedbackList) {
        request.setAttribute(FEEDBACK, feedbackList.get(FEEDBACK));
    }
}
