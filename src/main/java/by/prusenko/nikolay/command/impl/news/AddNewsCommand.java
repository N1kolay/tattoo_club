package by.prusenko.nikolay.command.impl.news;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.news.AddNewsService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code AddNewsCommand} class
 * is a command to add news by actor.
 *
 * @author Nikolay Prusenko
 */
public class AddNewsCommand extends BaseCommand {
    private static final List<String> REQUIRED_PARAMETERS= Arrays.asList(NEWS_TYPE,NEWS_TITLE,TEXT,ACTOR_ROLE);

    public AddNewsCommand(AddNewsService addNewsService) {
        super(addNewsService);
    }
    /**
     * Gets type of news, title of news, text, actor role. id of parlor, id of actor from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String type = request.getParameter(NEWS_TYPE);
        String title = request.getParameter(NEWS_TITLE);
        String text = request.getParameter(TEXT);
        String role = (String) request.getSession().getAttribute(ACTOR_ROLE);

        String idParlor = String.valueOf(request.getSession().getAttribute(ID_PARLOR));
        String idActor = String.valueOf(request.getSession().getAttribute(ID_ACTOR));
        return Map.of(NEWS_TYPE,type,NEWS_TITLE,title,TEXT,text,ACTOR_ROLE,role,ID_PARLOR,idParlor,ID_ACTOR,idActor);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }
    /**
     * Updates the request using the map of messages.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @param message -  map that has a {@code String} name of message as key and {@code String} message as value.
     */
    @Override
    protected void sendMessageRequest(HttpServletRequest request, Map<String, String> message) {
        request.getSession().setAttribute("addNewsMessage",message.get("addNewsMessage"));
    }
}
