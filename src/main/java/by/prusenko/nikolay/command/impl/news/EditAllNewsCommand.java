package by.prusenko.nikolay.command.impl.news;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.news.EditAllNewsService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code EditAllNewsCommand} class
 * is a command to edit posted(active) news.
 *
 * @author Nikolay Prusenko
 */
public class EditAllNewsCommand extends BaseCommand {

    private static final List<String> REQUIRED_PARAMETERS = Arrays.asList(ID_NEWS,NEWS_TYPE,BUTTON);

    public EditAllNewsCommand(EditAllNewsService editAllNewsService) {
        super(editAllNewsService);
    }
    /**
     * Gets id of news, type of news, name of button from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String newsId = String.valueOf(request.getParameter(ID_NEWS));
        String type = request.getParameter(NEWS_TYPE);
        String button = request.getParameter(BUTTON);
        return Map.of(ID_NEWS,newsId,NEWS_TYPE,type,BUTTON,button);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }

}
