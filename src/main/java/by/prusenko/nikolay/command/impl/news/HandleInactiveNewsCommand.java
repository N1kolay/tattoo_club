package by.prusenko.nikolay.command.impl.news;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.news.HandleInactiveNewsService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code HandleInactiveNewsCommand} class
 * is a command to edit not posted(inactive) news.
 *
 * @author Nikolay Prusenko
 */
public class HandleInactiveNewsCommand extends BaseCommand {
    private static final List<String> REQUIRED_PARAMETERS = Arrays.asList(NEWS_TYPE,BUTTON,NEWS_TITLE,TEXT,ID_NEWS);

    public HandleInactiveNewsCommand(HandleInactiveNewsService handleInactiveNewsService) {
        super(handleInactiveNewsService);
    }
    /**
     * Gets type of news, name of button, title of news, text, id of news from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String type = request.getParameter(NEWS_TYPE);
        String button = request.getParameter(BUTTON);
        String title = request.getParameter(NEWS_TITLE);
        String text = request.getParameter(TEXT);
        String id = request.getParameter(ID_NEWS);
        return Map.of(NEWS_TYPE,type,BUTTON,button,NEWS_TITLE,title,TEXT,text,ID_NEWS,id);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }

    }
