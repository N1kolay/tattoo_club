package by.prusenko.nikolay.command.impl.news;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.news.InactiveNewsService;
import by.prusenko.nikolay.util.Router;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code InactiveNewsCommand} class
 * is a command to show not posted(inactive) news.
 *
 * @author Nikolay Prusenko
 */
public class InactiveNewsCommand extends BaseCommand {
    private static final String INACTIVE_NEWS = "inactiveNews";
    private static final List<String>REQUIRED_PARAMETERS = Collections.singletonList(STATUS);

    public InactiveNewsCommand(InactiveNewsService inactiveNewsService) {
        super(inactiveNewsService);
    }
    /**
     * Gets status of news from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String status = request.getParameter(STATUS);
        return Map.of(STATUS,status);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }
    /**
     * Sets the router type to FORWARD
     *
     * @return a {@code Router.RouterType} object.
     */
    @Override
    protected Router.RouterType getRouterType() {
        return Router.RouterType.FORWARD;
    }
    /**
     * Updates the request using the map of attributes.
     *
     * @param request  an {@link HttpServletRequest} object that
     *                 contains the request the client has made of the servlet
     * @param listAttributes -  map that has a {@code String} name of parameter as key
     *                        and {@code List} parameter as value.
     */
    @Override
    protected void updateRequestList(HttpServletRequest request, Map<String, List> listAttributes) {
        request.setAttribute(INACTIVE_NEWS, listAttributes.get(INACTIVE_NEWS));
    }
}
