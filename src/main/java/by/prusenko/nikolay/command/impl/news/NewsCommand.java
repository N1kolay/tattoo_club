package by.prusenko.nikolay.command.impl.news;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.news.NewsService;
import by.prusenko.nikolay.util.Router;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code NewsCommand} class
 * is a command to show posted(active) news.
 *
 * @author Nikolay Prusenko
 */
public class NewsCommand extends BaseCommand {
    private static final List<String> REQUIRED_PARAMETERS = Collections.singletonList(NEWS_TYPE);
    private static final String LIST_OF_NEWS = "list";

    public NewsCommand(NewsService newsService) {
        super(newsService);
    }
    /**
     * Gets type of news from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String type = request.getParameter(NEWS_TYPE);
        return Map.of(NEWS_TYPE,type);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }
    /**
     * Sets the router type to FORWARD
     *
     * @return a {@code Router.RouterType} object.
     */
    @Override
    protected Router.RouterType getRouterType() {
        return Router.RouterType.FORWARD;
    }
    /**
     * Updates the request using the map of attributes.
     *
     * @param request  an {@link HttpServletRequest} object that
     *                 contains the request the client has made of the servlet
     * @param attributes -  map that has a {@code String} name of parameter as key
     *                        and {@code List} parameter as value.
     */
    @Override
    protected void updateRequestList(HttpServletRequest request, Map<String, List> attributes) {
        request.setAttribute(LIST_OF_NEWS, attributes.get(LIST_OF_NEWS));
    }
}
