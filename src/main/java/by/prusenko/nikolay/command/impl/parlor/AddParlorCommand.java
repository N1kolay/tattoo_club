package by.prusenko.nikolay.command.impl.parlor;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.parlor.AddParlorService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code AddParlorCommand} class
 * is a command to register parlor.
 *
 * @author Nikolay Prusenko
 */
public class AddParlorCommand extends BaseCommand {
    private static final List<String> REQUIRED_PARAMETERS = Arrays.asList(PARLOR_NAME, PARLOR_COUNTRY, PARLOR_CITY,
            PARLOR_ADDRESS, ACTOR_NAME);

    public AddParlorCommand(AddParlorService addParlorService) {
        super(addParlorService);
    }
    /**
     * Gets name of parlor, country, city, address, actor's name from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String parlor = request.getParameter(PARLOR_NAME);
        String country = request.getParameter(PARLOR_COUNTRY);
        String city = request.getParameter(PARLOR_CITY);
        String address = request.getParameter(PARLOR_ADDRESS);
        String name = (String) request.getSession().getAttribute(ACTOR_NAME);
        return Map.of(PARLOR_NAME, parlor, PARLOR_COUNTRY, country, PARLOR_CITY, city, PARLOR_ADDRESS, address,
                ACTOR_NAME, name);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }
    /**
     * Updates the request using the map of attributes.
     *
     * @param request  an {@link HttpServletRequest} object that
     *                 contains the request the client has made of the servlet
     * @param attributes -  map that has a {@code String} name of parameter as key
     *                        and {@code String} parameter as value.
     */
    @Override
    protected void updateRequest(HttpServletRequest request, Map<String, String> attributes) {
        request.getSession().setAttribute(ID_PARLOR, attributes.get(ID_PARLOR));
    }
}
