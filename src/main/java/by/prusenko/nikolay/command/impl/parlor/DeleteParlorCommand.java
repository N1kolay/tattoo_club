package by.prusenko.nikolay.command.impl.parlor;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.parlor.DeleteParlorService;
import by.prusenko.nikolay.util.Router;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code DeleteParlorCommand} class
 * is a command to delete registered parlor.
 *
 * @author Nikolay Prusenko
 */
public class DeleteParlorCommand extends BaseCommand {
    private static final List<String> REQUIRED_PARAMETERS = Collections.singletonList(ID_ACTOR);

    public DeleteParlorCommand(DeleteParlorService deleteParlorService) {
        super(deleteParlorService);
    }
    /**
     * Gets id of actor from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String idActor = String.valueOf(request.getSession().getAttribute(ID_ACTOR));
        return Map.of(ID_ACTOR,idActor);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }
    /**
     * Sets the router type to FORWARD
     *
     * @return a {@code Router.RouterType} object.
     */
    @Override
    public Router.RouterType getRouterType() {
        return Router.RouterType.FORWARD;
    }
    /**
     * Updates the request using the map of attributes.
     *
     * @param request  an {@link HttpServletRequest} object that
     *                 contains the request the client has made of the servlet
     * @param attributes -  map that has a {@code String} name of parameter as key
     *                        and {@code String} parameter as value.
     */
    @Override
    protected void updateRequest(HttpServletRequest request, Map<String, String> attributes) {
        request.getSession().setAttribute(ACTOR_ROLE,attributes.get(ACTOR_ROLE));
        request.getSession().setAttribute(ID_PARLOR,attributes.get(ID_PARLOR));
    }
    /**
     * Updates the request using the map of messages.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @param message -  map that has a {@code String} name of message as key and {@code String} message as value.
     */
    @Override
    protected void sendMessageRequest(HttpServletRequest request, Map<String, String> message) {
        request.setAttribute("deleteParlor",message.get("deleteParlor"));
    }
}
