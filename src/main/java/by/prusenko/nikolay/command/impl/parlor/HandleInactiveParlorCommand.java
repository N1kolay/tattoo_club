package by.prusenko.nikolay.command.impl.parlor;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.parlor.HandleInactiveParlorService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code HandleInactiveParlorCommand} class
 * is a command to edit not registered(inactive) parlor.
 *
 * @author Nikolay Prusenko
 */
public class HandleInactiveParlorCommand extends BaseCommand {
private static final List<String> REQUIRED_PARAMETERS = Arrays.asList(ID_PARLOR,BUTTON);

    public HandleInactiveParlorCommand(HandleInactiveParlorService handleInactiveParlorService) {
        super(handleInactiveParlorService);
    }
    /**
     * Gets id of parlor, name of button the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String idParlor = request.getParameter(ID_PARLOR);
        String button = request.getParameter(BUTTON);
        return Map.of(ID_PARLOR,idParlor,BUTTON,button);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }


}

