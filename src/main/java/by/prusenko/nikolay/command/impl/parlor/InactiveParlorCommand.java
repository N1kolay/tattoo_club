package by.prusenko.nikolay.command.impl.parlor;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.parlor.InactiveParlorService;
import by.prusenko.nikolay.util.Router;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code InactiveParlorCommand} class
 * is a command to show all not registered(inactive) parlor.
 *
 * @author Nikolay Prusenko
 */
public class InactiveParlorCommand extends BaseCommand {
    private static final String UNFIX_PARLOR = "unfixParlor";
    private static final List<String> REQUIRED_PARAMETERS = Collections.singletonList(STATUS);

    public InactiveParlorCommand(InactiveParlorService inactiveParlorService) {
        super(inactiveParlorService);
    }
    /**
     * Gets status of parlor from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String status = request.getParameter(STATUS);
        return Map.of(STATUS,status);
    }
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }
    /**
     * Sets the router type to FORWARD
     *
     * @return {@code Router.RouterType}
     */
    @Override
    protected Router.RouterType getRouterType() {
        return Router.RouterType.FORWARD;
    }
    /**
     * Updates the request using the map of attributes.
     *
     * @param request  an {@link HttpServletRequest} object that
     *                 contains the request the client has made of the servlet
     * @param listAttributes -  map that has a {@code String} name of parameter as key
     *                        and {@code List} parameter as value.
     */
    @Override
    protected void updateRequestList(HttpServletRequest request, Map<String, List> listAttributes) {
        request.setAttribute(UNFIX_PARLOR, listAttributes.get(UNFIX_PARLOR));
    }
}
