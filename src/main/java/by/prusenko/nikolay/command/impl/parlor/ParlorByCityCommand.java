package by.prusenko.nikolay.command.impl.parlor;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.parlor.ParlorByCityService;
import by.prusenko.nikolay.util.Router;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.PARLOR_CITY;
import static by.prusenko.nikolay.constant.Attribute.PARLOR_COUNTRY;

/**
 * The {@code ParlorByCityCommand} class
 * is a command to sort registered parlors by city.
 *
 * @author Nikolay Prusenko
 */
public class ParlorByCityCommand extends BaseCommand {
    private static final String LIST_OF_PARLORS = "list";
    private static final List<String> REQUIRED_PARAMETERS = Arrays.asList(PARLOR_COUNTRY, PARLOR_CITY);

    public ParlorByCityCommand(ParlorByCityService parlorByCityService) {
        super(parlorByCityService);
    }
    /**
     * Gets country, city from the request and
     * returns a map that has a {@code String} name of parameter as key and {@code String} parameter as value.
     *
     * @param request  an {@link HttpServletRequest} object that
     * contains the request the client has made of the servlet
     * @return  a Map - contains {@code String} parameters as value.
     */
    @Override
    protected Map<String, String> retrieveRequestParameters(HttpServletRequest request) {
        String country = request.getParameter(PARLOR_COUNTRY);
        String city = request.getParameter(PARLOR_CITY);
        return Map.of(PARLOR_COUNTRY, country, PARLOR_CITY, city);
    }
    /**
     * Returns a list {@code String} of required parameters.
     *
     * @return a list - contains names of parameters as {@code String}
     */
    @Override
    protected List<String> getRequiredParameters() {
        return REQUIRED_PARAMETERS;
    }
    /**
     * Sets the router type to FORWARD
     *
     * @return a {@code Router.RouterType} object.
     */
    @Override
    public Router.RouterType getRouterType() {
        return Router.RouterType.FORWARD;
    }
    /**
     * Updates the request using the map of attributes.
     *
     * @param request  an {@link HttpServletRequest} object that
     *                 contains the request the client has made of the servlet
     * @param listAttributes -  map that has a {@code String} name of parameter as key
     *                        and {@code List} parameter as value.
     */
    @Override
    protected void updateRequestList(HttpServletRequest request, Map<String, List> listAttributes) {
        request.setAttribute(LIST_OF_PARLORS, listAttributes.get(LIST_OF_PARLORS));
    }
}
