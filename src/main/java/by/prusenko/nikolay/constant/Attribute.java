package by.prusenko.nikolay.constant;

public final class Attribute {
    public static final String ACTOR_NAME = "name";
    public static final String ACTOR_EMAIL = "email";
    public static final String ACTOR_PASSWORD = "password";
    public static final String ACTOR_PASSWORD_CONFIRMATION = "confirmPassword";
    public static final String ACTOR_ROLE = "role";
    public static final String ID_ACTOR = "idActor";
    public static final String ID_PARLOR = "idParlor";
    public static final String TOKEN = "token";


    public static final String PARLOR_NAME = "parlorName";
    public static final String PARLOR_COUNTRY = "country";
    public static final String PARLOR_CITY = "city";
    public static final String PARLOR_ADDRESS = "address";
    public static final String FEEDBACK = "feedback";

    public static final String ANSWER = "answer";
    public static final String ID_FEEDBACK = "idFeedback";
    public static final String RATING = "rating";
    public static final String COMMENT = "comment";


    public static final String NEWS_TYPE = "type";
    public static final String ID_NEWS = "idNews";
    public static final String NEWS_TITLE = "name";
    public static final String TEXT = "text";

    public static final String LOCALE = "locale";
    public static final String LANG = "lang";
    public static final String EDIT = "edit";
    public static final String ADD = "add";
    public static final String BUTTON = "button";
    public static final String STATUS = "status";
    public static final String PATH = "path";

    private Attribute() {
    }

}
