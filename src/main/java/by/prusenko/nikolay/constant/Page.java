package by.prusenko.nikolay.constant;

public final class Page {
    public static final String INDEX = "/index.jsp";
    public static final String LOGIN = "/jsp/login.jsp";
    public static final String MAIN = "/jsp/main.jsp";
    public static final String PROFILE = "/jsp/actor/profile.jsp";
    public static final String SIGN_UP ="/jsp/signUp.jsp";
    public static final String INACTIVE_PARLOR = "/jsp/admin/inactiveParlors.jsp";
    public static final String PARLORS = "/jsp/parlors.jsp";
    public static final String NEWS = "/jsp/news.jsp";
    public static final String ADD_NEWS = "/jsp/actor/addNews.jsp";
    public static final String ARTICLES = "/jsp/article.jsp";
    public static final String DISCOUNTS = "/jsp/discount.jsp";
    public static final String INACTIVE_NEWS = "/jsp/admin/inactiveNews.jsp";
    public static final String FEEDBACK = "/jsp/feedback.jsp";
    public static final String MESSAGE = "/jsp/message.jsp";
    public static final String COMMAND= "/TattooClub?command=news&type=";
    public static final String FEEDBACK_COMMAND= "/TattooClub?command=feedback&idParlor=";
    public static final String INACTIVE_PARLOR_COMMAND= "/Admin?command=inactiveParlor&status=inactive";
    public static final String INACTIVE_NEWS_COMMAND= "/Admin?command=inactiveNews&status=inactive";

    private Page() {
    }
}
