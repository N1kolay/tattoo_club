package by.prusenko.nikolay.constant;

public final class SQLConstant {
    public static final String INSERT_USER = "INSERT INTO tattoo_club.actor (name, email, password) VALUES (?, ?, ?)";
    public static final String SELECT_USER_BY_EMAIL = "SELECT id, name, password, role, status, idParlor " +
            "from tattoo_club.actor WHERE email = ? and status = 'active'";
    public static final String UPDATE_USER = "UPDATE tattoo_club.actor SET login=?, hash_password=?, role=?, email=? " +
            "WHERE id=?";
    public static final String SELECT_BY_NAME = "SELECT name from tattoo_club.actor WHERE name =?";
    public static final String SELECT_BY_EMAIL = "SELECT email from tattoo_club.actor WHERE email =?";
    public static final String INSERT_PARLOR_ADDRESS = "INSERT INTO tattoo_club.address SET address = ?, " +
            "idCity = (SELECT id FROM tattoo_club.city WHERE cityName = ? AND idCountry = " +
            "(SELECT id FROM tattoo_club.country WHERE country = ?))";
    public static final String INSERT_PARLOR = "INSERT INTO tattoo_club.tattoo_parlor SET parlor=?, idAddress = " +
            "(SELECT id FROM tattoo_club.address WHERE address = ? AND idCity = (SELECT id FROM tattoo_club.city " +
            "WHERE cityName = ?))";
    public static final String UPDATE_USER_PARLOR = "UPDATE tattoo_club.actor SET idParlor = " +
            "(SELECT id FROM tattoo_club.tattoo_parlor WHERE parlor = ?) WHERE name = ?";
    public static final String SELECT_PARLOR_BY_USER_ROLE = "SELECT actor.name, parlor, tattoo_parlor.id, address, tattoo_parlor.status,\n" +
            "    cityName, country FROM \n" +
            "            (((( tattoo_club.tattoo_parlor JOIN tattoo_club.actor ON actor.idParlor = tattoo_parlor.id)\n" +
            "                                        JOIN tattoo_club.address ON idAddress=address.id) \n" +
            "                                        JOIN tattoo_club.city ON city.id=address.idCity)\n" +
            "                                        JOIN tattoo_club.country ON country.id = city.idCountry) WHERE role = 'user' and tattoo_parlor.status=?";
    public static final String UPDATE_PARLOR_STATUS_TO_ACTIVE = "UPDATE tattoo_club.tattoo_parlor SET status = 'active' " +
            "WHERE id=?";
    public static final String UPDATE_USER_ROLE_TO_PARLOR = "UPDATE tattoo_club.actor SET role = 'parlor' " +
            "WHERE idParlor=?";
    public static final String DELETE_PARLOR_BY_ID = "DELETE FROM tattoo_club.tattoo_parlor WHERE id=?";
    public static final String UPDATE_USER_ID_PARLOR_TO_NULL = "UPDATE tattoo_club.actor SET idParlor = NULL " +
            "WHERE idParlor=?";
    public static final String SELECT_ACTIVE_NEWS_BY_TYPE = "SELECT news.id, news.name, newsType, text, additionDate, actor.name" +
            " as author FROM tattoo_club.news JOIN tattoo_club.actor ON news.idActor = actor.id WHERE news.status = 'active' " +
            "and newsType = ? UNION SELECT news.id, news.name, newsType, text, additionDate, parlor as author " +
            "FROM tattoo_club.news JOIN tattoo_club.tattoo_parlor ON news.idParlor = tattoo_parlor.id " +
            "WHERE news.status = 'active' and newsType = ? ORDER BY additionDate DESC";
    public static final String UPDATE_NEWS_TO_INACTIVE = "UPDATE tattoo_club.news SET status = 'inactive' WHERE id = ?";
    public static final String INSERT_INTO_NEWS_ACTOR = "INSERT INTO tattoo_club.news (name, newsType, text,idActor) " +
            "VALUES(?,?,?,?)";
    public static final String INSERT_INTO_NEWS_PARLOR = "INSERT INTO tattoo_club.news (name, newsType, text,idParlor) " +
            "VALUES(?,?,?,?)";
    public static final String SELECT_ALL_INACTIVE_NEWS = "SELECT news.id, news.name, newsType, text, actor.name as author," +
            " additionDate FROM tattoo_club.news JOIN tattoo_club.actor ON news.idActor=actor.id WHERE news.status=?\n" +
            "UNION SELECT news.id, news.name, newsType, text, parlor as author, additionDate FROM tattoo_club.news JOIN " +
            "tattoo_club.tattoo_parlor ON news.idParlor=tattoo_parlor.id WHERE news.status=? ORDER BY additionDate asc";
    public final static String UPDATE_NEWS_TO_ACTIVE = "UPDATE tattoo_club.news SET name=?, newsType=?, text=?, status='active' where id = ?";
    public final static String DELETE_NEWS = "DELETE FROM tattoo_club.news where id=?";
    public final static String DELETE_PARLOR_FROM_ACTOR = "UPDATE tattoo_club.actor SET actor.idParlor=null, actor.role='user' WHERE actor.id=?";
    public final static String SELECT_ACTIVE_PARLORS = "SELECT tattoo_parlor.id, parlor,address,cityName,country,status " +
            "FROM tattoo_club.tattoo_parlor JOIN  tattoo_club.address ON idAddress=address.id JOIN tattoo_club.city ON " +
            "address.idCity=city.id JOIN tattoo_club.country ON city.idCountry=country.id WHERE status=?";
    public final static String SELECT_FEEDBACK_BY_IDPARLOR = "SELECT feedback.id, comment, answer, feedback.rating, " +
            "feedback.idActor,feedback.idParlor, feedback.additionDate,actor.name, tattoo_parlor.parlor FROM tattoo_club.feedback" +
            " LEFT JOIN tattoo_club.answer ON feedback.id=answer.idFeedback JOIN tattoo_club.actor ON actor.id=feedback.idActor " +
            "JOIN tattoo_club.tattoo_parlor ON tattoo_parlor.id=feedback.idParlor WHERE feedback.idParlor=? ORDER BY additionDate DESC ";
    public final static String INSERT_ANSWER = "INSERT INTO tattoo_club.answer (answer, idFeedback, idActor) values (?,?,?)";
    public final static String SELECT_ACTIVE_PARLOR_BY_CITY = "SELECT tattoo_parlor.id, tattoo_parlor.parlor, address.address, " +
            "city.cityName, country.country,tattoo_parlor.status FROM ((tattoo_club.tattoo_parlor JOIN tattoo_club.address ON " +
            "tattoo_parlor.idAddress=address.id) JOIN city ON address.idCity=city.id JOIN country ON city.idCountry=country.id) " +
            "WHERE country.country=? and city.cityName=? and status='active'";
    public final static String INSERT_FEEDBACK = "INSERT INTO tattoo_club.feedback(comment,rating,idActor,idParlor) values(?,?,?,?)";
    public final static String DELETE_ANSWER = "DELETE FROM tattoo_club.answer WHERE idFeedback = ?";
    public final static String DELETE_FEEDBACK = "DELETE FROM tattoo_club.feedback WHERE id = ?";
    public final static String SELECT_PARLOR_ID_BY_ADDRESS = "SELECT tattoo_parlor.id FROM tattoo_club.tattoo_parlor " +
            "JOIN tattoo_club.address ON tattoo_parlor.idAddress=address.id where parlor=? and address=?";
    public final static String UPDATE_USER_STATUS_TO_ACTIVE = "UPDATE tattoo_club.actor SET status='active' where name=?";

    private SQLConstant() {
    }
}
