package by.prusenko.nikolay.controller;

import by.prusenko.nikolay.command.SimpleCommand;
import by.prusenko.nikolay.command.factory.ActionFactory;
import by.prusenko.nikolay.util.Router;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The {@code ParlorController} class
 * is a main HttpServlet for current Web project.
 * Overrides doPost and doGet methods by calling
 * the own method processRequest(request, response).
 *
 * @author Nikolay Prusenko
 */

@WebServlet ("/TattooClub")
public class ParlorController extends HttpServlet {
    private static final ActionFactory ACTION_FACTORY = new ActionFactory();

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        processRequest(req,resp);
    }

    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        processRequest(req,resp);
    }
    private void processRequest(HttpServletRequest request,HttpServletResponse response) throws IOException, ServletException {
        SimpleCommand command = ACTION_FACTORY.defineCommand(request);

            Router router = command.execute(request);

        if (router.getType() == Router.RouterType.FORWARD) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(router.getPath());
            dispatcher.forward(request, response);
        }
        else{
            response.sendRedirect(router.getPath());
        }

    }
}
