package by.prusenko.nikolay.dao;

import by.prusenko.nikolay.entity.Feedback;
import by.prusenko.nikolay.exception.DaoException;

import java.util.List;

/**
 * The {@code FeedbackDao} interface
 * for {@code FeedbackDaoIml}
 * provides access to the database
 *
 * @author Nikolay Prusenko
 */
public interface FeedbackDao {
    /**
     * Finds all parlor's feedback in database
     * @return a list contains {@code Feedback}, not null
     * @throws DaoException if error (database access) occurs
     */
    List<Feedback> findFeedbackByParlor(int id)throws DaoException;
    /**
     * Adds answer to feedback in database
     * @throws DaoException if error (database access) occurs
     */
    void addAnswer(String answer, int idFeedback, int idActor)throws DaoException;
    /**
     * Adds feedback to parlor in database
     * @throws DaoException if error (database access) occurs
     */
    void addFeedback(String comment,String rating,int idActor,String idParlor)throws DaoException;
    /**
     * Deletes answer and feedback in database
     * @throws DaoException if error (database access) occurs
     */
    void deleteAnswerFeedback(int id)throws DaoException;
    /**
     * Deletes feedback in database
     * @throws DaoException if error (database access) occurs
     */
    void deleteFeedback(int id)throws DaoException;
}
