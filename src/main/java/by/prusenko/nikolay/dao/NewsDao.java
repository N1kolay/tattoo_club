package by.prusenko.nikolay.dao;

import by.prusenko.nikolay.entity.News;
import by.prusenko.nikolay.exception.DaoException;

import java.util.List;

/**
 * The {@code NewsDao} interface
 * for {@code NewsDaoIml}
 * provides access to the database
 *
 * @author Nikolay Prusenko
 */
public interface NewsDao {
    /**
     * Finds posted(active) news in database
     * @return a list contains {@code News}, not null
     * @throws DaoException if error (database access) occurs
     */
    List<News> findActiveNews(String type) throws DaoException;
    /**
     * Deletes posted(active) news from database
     * @throws DaoException if error (database access) occurs
     */
    void deleteNews(int id)throws DaoException;
    /**
     * Changes new's status to inactive(unposted) in database
     * @throws DaoException if error (database access) occurs
     */
    void inactivateNews(int id)throws DaoException;
    /**
     * Adds news in database
     * @throws DaoException if error (database access) occurs
     */
    void addNews(String sql,String name,String type,String text,int id)throws DaoException;
    /**
     * Finds unposted(inactive) news in database
     * @return a list contains {@code News}, not null
     * @throws DaoException if error (database access) occurs
     */
    List<News> findInactiveNews(String status) throws DaoException;
}
