package by.prusenko.nikolay.dao;

import by.prusenko.nikolay.entity.Parlor;
import by.prusenko.nikolay.entity.User;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;

import java.util.List;

/**
 * The {@code ParlorDao} interface
 * for {@code ParlorDaoIml}
 * provides access to the database
 *
 * @author Nikolay Prusenko
 */
public interface ParlorDao {

    void chainParlorUser(String name, String parlor, String country, String city, String address) throws DaoException;
    /**
     * Finds unregistered(inactive) parlors in database
     * @return a list contains {@code User}, not null
     * @throws DaoException if error (database access) occurs
     */
    List<User> findInactiveParlor(String status)throws DaoException;
    /**
     *Registers(activates) parlor in database
     * @throws DaoException if error (database access) occurs
     */
    void activateParlor(int id)throws DaoException;
    /**
     * Deletes unregistered(inactive) parlors from database
     * @throws DaoException if error (database access) occurs
     */
    void deleteIncorrectParlor (int id)throws DaoException;
    /**
     * Deletes registered(active) parlors from database
     * @throws DaoException if error (database access) occurs
     */
    void deleteParlorFromUser(int idActor)throws DaoException;
    /**
     * Finds registered(active) parlors in database
     *
     * @return a list contains {@code Parlor}, not null
     * @throws DaoException if error (database access) occurs
     */
    List<Parlor> findActiveParlor(String status)throws DaoException;
    /**
     * Finds parlors by country and city in database
     *
     * @return a list contains {@code Parlor}, not null
     * @throws DaoException if error (database access) occurs
     */
    List<Parlor> findParlorByCity(String country,String city)throws DaoException;
    /**
     * Finds parlors by address
     *
     * @return parlor's id
     * @throws DaoException if error (database access) occurs
     */
    int findParlorByAddress(String parlor, String address)throws DaoException;

}
