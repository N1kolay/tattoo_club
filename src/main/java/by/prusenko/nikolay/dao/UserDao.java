package by.prusenko.nikolay.dao;

import by.prusenko.nikolay.entity.User;
import by.prusenko.nikolay.exception.DaoException;

import java.util.Optional;

/**
 * The {@code UserDao} interface
 * for {@code UserDaoIml}
 * provides access to the database
 *
 * @author Nikolay Prusenko
 */
public interface UserDao {
    /**
     * Adds user in database
     *
     * @return {@code true} if user is added, {@code false} if is not.
     * @throws DaoException if error (database access) occurs
     */
    boolean addUser(User user) throws DaoException;
    /**
     * Checks a user's name in database for uniqueness
     *
     * @return {@code true} if user's name is unique, {@code false} if is not.
     * @throws DaoException if error (database access) occurs
     */
    boolean uniqueName(String name) throws DaoException;
    /**
     * Checks a user's email in database for uniqueness
     *
     * @return {@code true} if user's email is unique, {@code false} if is not.
     * @throws DaoException if error (database access) occurs
     */
    boolean uniqueEmail(String email) throws DaoException;
    /**
     * @see UserDao#uniqueName(String)
     * @see UserDao#uniqueEmail(String)
     *
     * @return {@code Optional}
     * @throws DaoException if error (database access) occurs
     */
    boolean uniqueNameEmail(String name, String email) throws DaoException;
    /**
     * Finds user in database
     *
     * @return {@code Optional}
     * @throws DaoException if error (database access) occurs
     */
    Optional<User> findUser(String email, String password) throws DaoException;
    /**
     * Updates user's status in database
     * @throws DaoException if error (database access) occurs
     */
    void updateStatus(String name) throws DaoException;

}
