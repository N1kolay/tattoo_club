package by.prusenko.nikolay.dao.impl;

import by.prusenko.nikolay.dao.FeedbackDao;
import by.prusenko.nikolay.dao.pool.ConnectionPool;
import by.prusenko.nikolay.entity.Feedback;
import by.prusenko.nikolay.exception.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.prusenko.nikolay.constant.SQLConstant.*;

/**
 * The {@code FeedbackDaoImpl} class
 * provides access to the tables 'feedback' in the database
 *
 * @author Nikolay Prusenko
 */
public class FeedbackDaoImpl implements FeedbackDao {
    private static final ConnectionPool CONNECTION_POOL = ConnectionPool.getInstance();

    @Override
    public List<Feedback> findFeedbackByParlor(int id) throws DaoException {
        Connection connection = null;
        List<Feedback> listFeedback = new ArrayList<>();
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_FEEDBACK_BY_IDPARLOR)) {
                preparedStatement.setInt(1, id);
                try (ResultSet rs = preparedStatement.executeQuery()) {
                    while (rs.next()) {
                        Feedback feedback = new Feedback.Builder().setId(rs.getInt("id")).setAuthor(rs.getString("name"))
                                .setComment(rs.getString("comment")).setRating(rs.getInt("rating"))
                                .setIdActor(rs.getInt("idActor")).setParlor(rs.getString("parlor"))
                                .setIdParlor(rs.getInt("idParlor")).setAnswer(rs.getString("answer"))
                                .buildFeedback();
                        listFeedback.add(feedback);
                    }
                    return listFeedback;
                }
            }
        } catch (SQLException e) {
            throw new DaoException("Find feedback by parlor - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }

    @Override
    public void addAnswer(String answer, int idFeedback, int idActor) throws DaoException {
        Connection connection = null;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement ps = connection.prepareStatement(INSERT_ANSWER)) {
                ps.setString(1, answer);
                ps.setInt(2, idFeedback);
                ps.setInt(3, idActor);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("Add answer - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }

    @Override
    public void addFeedback(String comment, String rating, int idActor, String idParlor) throws DaoException {
        Connection connection = null;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement ps = connection.prepareStatement(INSERT_FEEDBACK)) {
                ps.setString(1, comment);
                ps.setString(2, rating);
                ps.setInt(3, idActor);
                ps.setString(4, idParlor);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("Add feedback - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }

    @Override
    public void deleteAnswerFeedback(int id) throws DaoException {
        Connection connection = null;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            connection.setAutoCommit(false);
            try (PreparedStatement ps = connection.prepareStatement(DELETE_ANSWER);
                 PreparedStatement ps1 = connection.prepareStatement(DELETE_FEEDBACK)) {
                ps.setInt(1, id);
                ps1.setInt(1, id);
                if (ps.executeUpdate() == 1 && ps1.executeUpdate() == 1) {
                    connection.commit();
                } else {
                    throw new SQLException();
                }
            } catch (SQLException e) {
                connection.rollback();
                connection.setAutoCommit(true);
                throw new DaoException("Delete answer & feedback - Error in Statement", e);
            }
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }

    @Override
    public void deleteFeedback(int id) throws DaoException {
        Connection connection = null;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_FEEDBACK)) {
                preparedStatement.setInt(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("Delete feedback - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }
}
