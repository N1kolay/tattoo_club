package by.prusenko.nikolay.dao.impl;

import by.prusenko.nikolay.dao.NewsDao;
import by.prusenko.nikolay.dao.pool.ConnectionPool;
import by.prusenko.nikolay.entity.News;
import by.prusenko.nikolay.exception.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import static by.prusenko.nikolay.constant.SQLConstant.*;

/**
 * The {@code NewsDaoImpl} class
 * provides access to the tables 'news' in the database
 *
 * @author Nikolay Prusenko
 */
public class NewsDaoImpl implements NewsDao {
    private static final ConnectionPool CONNECTION_POOL = ConnectionPool.getInstance();


    @Override
    public List<News> findActiveNews(String type) throws DaoException {
        Connection connection = null;
        List<News> list = new ArrayList<>();
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ACTIVE_NEWS_BY_TYPE)) {
                preparedStatement.setString(1, type);
                preparedStatement.setString(2, type);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    while (resultSet.next()) {
                        News news = new News();
                        news.setId(resultSet.getInt("id"));
                        news.setName(resultSet.getString("name"));
                        news.setText(resultSet.getString("text"));
                        news.setType(resultSet.getString("newsType"));
                        news.setDate(new SimpleDateFormat("dd/MM/yy").format(resultSet.getDate("additionDate")));
                        news.setAuthor(resultSet.getString("author"));
                        list.add(news);
                    }
                }
            }
        } catch (SQLException e) {
            throw new DaoException("find News - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
        return list;
    }


    @Override
    public void deleteNews(int id) throws DaoException {
        Connection connection = null;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_NEWS)) {
                preparedStatement.setInt(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("delete news - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }

    @Override
    public void inactivateNews(int id) throws DaoException {
        Connection connection = null;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_NEWS_TO_INACTIVE)) {
                preparedStatement.setInt(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("inactive news - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }

    @Override
    public void addNews(String sql, String name, String type, String text, int id) throws DaoException {
        Connection connection = null;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {
                preparedStatement.setString(1, name);
                preparedStatement.setString(2, type);
                preparedStatement.setString(3, text);
                preparedStatement.setInt(4, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("add news - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }

    @Override
    public List<News> findInactiveNews(String status) throws DaoException {
        Connection connection = null;
        List<News> list = new ArrayList<>();
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try(PreparedStatement preparedStatement =  connection.prepareStatement(SELECT_ALL_INACTIVE_NEWS)) {
                preparedStatement.setString(1,status);
                preparedStatement.setString(2,status);
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    News news = new News();
                    news.setId(rs.getInt("id"));
                    news.setName(rs.getString("name"));
                    news.setAuthor(rs.getString("author"));
                    news.setText(rs.getString("text"));
                    news.setType(rs.getString("newsType"));
                    news.setDate(new SimpleDateFormat("dd/MM/yy").format(rs.getDate("additionDate")));
                    list.add(news);
                }
            }
        } catch (SQLException e) {
            throw new DaoException("find inactive News - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
        return list;
    }


}
