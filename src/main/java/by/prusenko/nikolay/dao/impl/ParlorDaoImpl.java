package by.prusenko.nikolay.dao.impl;

import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.dao.pool.ConnectionPool;
import by.prusenko.nikolay.entity.Parlor;
import by.prusenko.nikolay.entity.User;
import by.prusenko.nikolay.exception.DaoException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.prusenko.nikolay.constant.SQLConstant.*;

/**
 * The {@code ParlorDaoImpl} class
 * provides access to the tables 'tattoo_parlor' in the database
 *
 * @author Nikolay Prusenko
 */
public class ParlorDaoImpl implements ParlorDao {

    private static final ConnectionPool CONNECTION_POOL = ConnectionPool.getInstance();


    @Override
    public void chainParlorUser(String name, String parlor, String country, String city, String address) throws DaoException {
        Connection connection = null;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            connection.setAutoCommit(false);
            try (PreparedStatement ps1 = connection.prepareStatement(INSERT_PARLOR_ADDRESS);
                 PreparedStatement ps2 = connection.prepareStatement(INSERT_PARLOR);
                 PreparedStatement ps3 = connection.prepareStatement(UPDATE_USER_PARLOR)) {
                ps1.setString(1, address);
                ps1.setString(2, city);
                ps1.setString(3, country);
                ps1.executeUpdate();
                ps2.setString(1, parlor);
                ps2.setString(2, address);
                ps2.setString(3, city);
                ps2.executeUpdate();
                ps3.setString(1, parlor);
                ps3.setString(2, name);
                ps3.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                connection.rollback();
                connection.setAutoCommit(true);
                throw new DaoException("Chain Parlor with User - Error in Statement", e);
            }
            connection.setAutoCommit(true);
        } catch (SQLException e) {
            throw new DaoException(e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }


    @Override
    public List<User> findInactiveParlor(String status) throws DaoException {
        Connection connection = null;
        List<User> listParlor = new ArrayList<>();
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PARLOR_BY_USER_ROLE)) {
                preparedStatement.setString(1, status);
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    User user = new User();
                    Parlor parlor = new Parlor();
                    user.setName(rs.getString("name"));
                    parlor.setId(rs.getInt("id"));
                    parlor.setName(rs.getString("parlor"));
                    parlor.setAddress(rs.getString("address"));
                    parlor.setCity(rs.getString("cityName"));
                    parlor.setCountry(rs.getString("country"));
                    user.setParlor(parlor);
                    listParlor.add(user);
                }
                return listParlor;
            }
        } catch (SQLException e) {
            throw new DaoException("Show inactive parlor - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }

    @Override
    public void activateParlor(int id) throws DaoException {
        Connection connection = null;
        appointParlorRole(id);
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_PARLOR_STATUS_TO_ACTIVE)) {
                preparedStatement.setInt(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("Activate Parlor  - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }


    private boolean appointParlorRole(int id) throws DaoException {
        Connection connection = null;
        boolean result;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_ROLE_TO_PARLOR)) {
                preparedStatement.setInt(1, id);
                result = preparedStatement.executeUpdate() == 1;
                return result;
            }
        } catch (SQLException e) {
            throw new DaoException("appoint parlor role  - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }

    @Override
    public void deleteIncorrectParlor(int id) throws DaoException {
        Connection connection = null;
        unchainUserParlor(id);
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_PARLOR_BY_ID)) {
                preparedStatement.setInt(1, id);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("delete Incorrect Parlor  - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }

    }


    private boolean unchainUserParlor(int id) throws DaoException {
        Connection connection = null;
        boolean result;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_ID_PARLOR_TO_NULL)) {
                preparedStatement.setInt(1, id);
                result = preparedStatement.executeUpdate() == 1;
                return result;
            }
        } catch (SQLException e) {
            throw new DaoException("unchain User Parlor  - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }

    @Override
    public void deleteParlorFromUser(int idActor) throws DaoException {
        Connection connection = null;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(DELETE_PARLOR_FROM_ACTOR)) {
                preparedStatement.setInt(1, idActor);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("delete parlor from user - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }

    @Override
    public List<Parlor> findActiveParlor(String status) throws DaoException {
        Connection connection = null;
        List<Parlor> listParlor = new ArrayList<>();
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ACTIVE_PARLORS)) {
                preparedStatement.setString(1, status);
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    Parlor parlor = new Parlor();
                    parlor.setId(rs.getInt("id"));
                    parlor.setName(rs.getString("parlor"));
                    parlor.setAddress(rs.getString("address"));
                    parlor.setCity(rs.getString("cityName"));
                    parlor.setCountry(rs.getString("country"));
                    listParlor.add(parlor);
                }
                return listParlor;
            }
        } catch (SQLException e) {
            throw new DaoException("Find active parlor - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }

    }


    @Override
    public List<Parlor> findParlorByCity(String country, String city) throws DaoException {
        Connection connection = null;
        List<Parlor> listParlor = new ArrayList<>();
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement ps = connection.prepareStatement(SELECT_ACTIVE_PARLOR_BY_CITY)) {
                ps.setString(1, country);
                ps.setString(2, city);
                ResultSet rs = ps.executeQuery();
                while (rs.next()) {
                    Parlor parlor = new Parlor();
                    parlor.setId(rs.getInt("id"));
                    parlor.setName(rs.getString("parlor"));
                    parlor.setCity(rs.getString("cityName"));
                    parlor.setAddress(rs.getString("address"));
                    parlor.setCountry(rs.getString("country"));
                    listParlor.add(parlor);
                }
                return listParlor;
            }
        } catch (SQLException e) {
            throw new DaoException("Find parlor by city - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }

    }


    @Override
    public int findParlorByAddress(String parlor, String address) throws DaoException {
        Connection connection = null;
        int parlorId = 0;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_PARLOR_ID_BY_ADDRESS)) {
                preparedStatement.setString(1, parlor);
                preparedStatement.setString(2, address);
                ResultSet rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    parlorId = rs.getInt(1);
                }
                return parlorId;
            }
        } catch (SQLException e) {
            throw new DaoException("Find Parlor By Address - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }


}
