package by.prusenko.nikolay.dao.impl;

import by.prusenko.nikolay.dao.UserDao;
import by.prusenko.nikolay.dao.pool.ConnectionPool;
import by.prusenko.nikolay.entity.ActorRole;
import by.prusenko.nikolay.entity.User;
import by.prusenko.nikolay.exception.DaoException;
import org.mindrot.jbcrypt.BCrypt;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

import static by.prusenko.nikolay.constant.SQLConstant.*;

/**
 * The {@code UserDaoImpl} class
 * provides access to the tables 'actor' in the database
 *
 * @author Nikolay Prusenko
 */
public class UserDaoImpl implements UserDao {

    private static final ConnectionPool CONNECTION_POOL = ConnectionPool.getInstance();

    @Override
    public boolean addUser(User user) throws DaoException {
        Connection connection = null;
        boolean result;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(INSERT_USER)) {
                preparedStatement.setString(1, user.getName());
                preparedStatement.setString(2, user.getEmail());
                preparedStatement.setString(3, user.getPassword());
                result = preparedStatement.executeUpdate() == 1;
                return result;
            }
        } catch (SQLException e) {
            throw new DaoException("Create user - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }


    @Override
    public Optional<User> findUser(String email, String password) throws DaoException {
        Connection connection = null;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_USER_BY_EMAIL)) {
                preparedStatement.setString(1, email);
                try (ResultSet resultSet = preparedStatement.executeQuery()) {
                    if (resultSet.next()) {
                        if (BCrypt.checkpw(password, resultSet.getString("password"))) {
                            User user = new User();
                            user.setName(resultSet.getString("name"));
                            user.setRole(ActorRole.valueOf(resultSet.getString("role").toUpperCase()));
                            user.setIdParlor(resultSet.getInt("idParlor"));
                            user.setIdActor(resultSet.getInt("id"));
                            return Optional.of(user);
                        } else {
                            return Optional.empty();
                        }
                    }
                }
            }
            return Optional.empty();
        } catch (SQLException e) {
            throw new DaoException("Find user by email and password - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }


    @Override
    public boolean uniqueNameEmail(String name, String email) throws DaoException {
        return (uniqueName(name) && uniqueEmail(email));
    }


    @Override
    public boolean uniqueName(String name) throws DaoException {
        Connection connection = null;
        boolean result;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_NAME)) {
                preparedStatement.setString(1, name);
                result = !(preparedStatement.executeQuery().next());
                return result;
            }
        } catch (SQLException e) {
            throw new DaoException("Unique name - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }

    @Override
    public boolean uniqueEmail(String email) throws DaoException {
        Connection connection = null;
        boolean result;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BY_EMAIL)) {
                preparedStatement.setString(1, email);
                result = !(preparedStatement.executeQuery().next());
                return result;
            }
        } catch (SQLException e) {
            throw new DaoException("Unique e-mail - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }

    }

    @Override
    public void updateStatus(String name) throws DaoException {
        Connection connection = null;
        try {
            connection = CONNECTION_POOL.receiveConnection();
            try (PreparedStatement preparedStatement = connection.prepareStatement(UPDATE_USER_STATUS_TO_ACTIVE)) {
                preparedStatement.setString(1, name);
                preparedStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new DaoException("Update user status - Error ", e);
        } finally {
            CONNECTION_POOL.returnConnection(connection);
        }
    }
}
