package by.prusenko.nikolay.dao.pool;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;


public class ConnectionPool {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String PROPERTY_FILE = "/ConnectionPool.properties";
    private static final AtomicBoolean IS_CREATED = new AtomicBoolean(false);
    private static ReentrantLock threadLock = new ReentrantLock();
    private static ConnectionPool connectionPool;
    private BlockingQueue<Connection> queueOfConnections;

    private int poolSize;


    public static ConnectionPool getInstance() {
        if (!IS_CREATED.get()) {
            threadLock.lock();
            try {
                if (connectionPool == null) {
                    connectionPool = new ConnectionPool();
                    IS_CREATED.getAndSet(true);
                }
            } finally {
                threadLock.unlock();
            }
        }
        return connectionPool;
    }

    private ConnectionPool() {
        Connection tempConnection;
        InputStream propertyFile = getClass().getResourceAsStream(PROPERTY_FILE);
        Properties props = new Properties();
        try {
            props.load(propertyFile);
            propertyFile.close();
        } catch (IOException exc) {
            LOGGER.log(Level.ERROR, "Error reading property file. " + exc +
                    ".properties is in the same directory with the ConnectionPool class.");
            return;
        }

        try {
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
            String url = props.getProperty("url");
            String userName = props.getProperty("userName");
            String password = props.getProperty("password");
            poolSize = Integer.parseInt(props.getProperty("numberOfConnections"));
            queueOfConnections = new LinkedBlockingQueue<>(poolSize);

            for (int i = 0; i < poolSize; i++) {
                tempConnection = DriverManager.getConnection(url, userName, password);
                queueOfConnections.put(tempConnection);
            }
        } catch (SQLException | InterruptedException e) {
            LOGGER.log(Level.ERROR, "Unable to setup JDBC connection pool. " + e);
        }
    }


    public Connection receiveConnection() {
        Connection connection = null;
        try {
            connection = queueOfConnections.take();
        } catch (InterruptedException e) {
            LOGGER.error("Couldn't take connection from connection pool", e);
        }
        return connection;
    }

    public void returnConnection(Connection connection) {
        if (connection != null) {
            try {
                if (!connection.isClosed()) {
                    queueOfConnections.put(connection);
                }
            } catch (SQLException | InterruptedException e) {
                LOGGER.log(Level.ERROR, e);
            }
        }
    }

    public void closePool() {
        try {
            for (int i = 0; i < poolSize; i++) {
                queueOfConnections.take().close();
            }
        } catch (SQLException | InterruptedException e) {
            LOGGER.error("Couldn't close connection pool", e);
        }
    }

}

