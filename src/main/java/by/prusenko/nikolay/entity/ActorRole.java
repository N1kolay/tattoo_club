package by.prusenko.nikolay.entity;

/**
 * The {@code ActorRole} class
 * is enum that represents actor role.
 *
 * @author Nikolay Prusenko
 */
public enum ActorRole {
    ADMIN("admin"), MODERATOR("moderator"), USER("user"), PARLOR("parlor"),;
    private String value;

    ActorRole(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }
}
