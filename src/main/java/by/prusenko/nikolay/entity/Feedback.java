package by.prusenko.nikolay.entity;

/**
 * The {@code Feedback} class
 * is an entity that represents table 'feedback' in the database.
 *
 * @author Nikolay Prusenko
 */
public class Feedback {
    private int id;
    private String comment;
    private int rating;
    private String date;
    private int idActor;
    private int idParlor;
    private String author;
    private String parlor;
    private String answer;


    public int getId() {
        return id;
    }
    public String getComment() {
        return comment;
    }
    public int getRating() {
        return rating;
    }
    public String getDate() {
        return date;
    }
    public int getIdActor() {
        return idActor;
    }
    public int getIdParlor() {
        return idParlor;
    }
    public String getAuthor() {
        return author;
    }
    public String getParlor() {
        return parlor;
    }
    public String getAnswer() {
        return answer;
    }

    private Feedback(Builder builder){
        this.id=builder.id;
        this.comment=builder.comment;
        this.rating=builder.rating;
        this.date=builder.date;
        this.idActor=builder.idActor;
        this.idParlor=builder.idParlor;
        this.author=builder.author;
        this.parlor=builder.parlor;
        this.answer=builder.answer;
    }

    public static class Builder{
        private int id;
        private String comment;
        private int rating;
        private String date;
        private int idActor;
        private int idParlor;
        private String author;
        private String parlor;
        private String answer;


        public Builder setId(int id) {
            this.id = id;
            return this;
        }
        public Builder setComment(String comment) {
            this.comment = comment;
            return this;
        }
        public Builder setRating(int rating) {
            this.rating = rating;
            return this;
        }
        public Builder setDate(String date) {
            this.date = date;
            return this;
        }
        public Builder setIdActor(int idActor) {
            this.idActor = idActor;
            return this;
        }
        public Builder setIdParlor(int idParlor) {
            this.idParlor = idParlor;
            return this;
        }
        public Builder setAuthor(String author) {
            this.author = author;
            return this;
        }
        public Builder setParlor(String parlor) {
            this.parlor = parlor;
            return this;
        }
        public Builder setAnswer(String answer) {
            this.answer = answer;
            return this;}

        public Feedback buildFeedback(){
            return  new Feedback(this);
        }
    }


}
