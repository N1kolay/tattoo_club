package by.prusenko.nikolay.entity;

/**
 * The {@code News} class
 * is an entity that represents table 'news' in the database.
 *
 * @author Nikolay Prusenko
 */
public class News {
    private int id;
    private String name;
    private String text;
    private String type;
    private String date;
    private String author;

    public News() { }

    public News(int id,String name, String text, String type, String date, String author) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.type = type;
        this.date = date;
        this.author = author;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthor() { return author; }

    public void setAuthor(String author) { this.author = author;}
}
