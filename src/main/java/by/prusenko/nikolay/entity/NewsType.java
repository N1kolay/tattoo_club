package by.prusenko.nikolay.entity;

/**
 * The {@code NewsType} class
 * is enum that represents news type.
 *
 * @author Nikolay Prusenko
 */
public enum NewsType {
    NEWS("news"),ARTICLE("article"),DISCOUNT("discount");

    private String type;

    NewsType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return type;
    }
}
