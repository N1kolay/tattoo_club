package by.prusenko.nikolay.entity;

/**
 * The {@code Parlor} class
 * is an entity that represents table 'tattoo_parlor' in the database.
 *
 * @author Nikolay Prusenko
 */
public class Parlor {
    private int id;
    private String name;
    private String address;
    private String city;
    private String country;


    public Parlor() {
    }

    public Parlor(String name, String address, String city, String country) {
        this.name = name;
        this.address = address;
        this.city = city;
        this.country = country;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getCity() {
        return city;
    }

    public String getCountry() {
        return country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
