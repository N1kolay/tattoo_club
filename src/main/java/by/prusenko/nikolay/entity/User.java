package by.prusenko.nikolay.entity;

import java.util.Objects;

/**
 * The {@code User} class
 * is an entity that represents table 'actor' in the database.
 *
 * @author Nikolay Prusenko
 */
public class User {
    private String name;
    private String email;
    private String password;
    private ActorRole role;
    private Parlor parlor;
    private boolean status;
    private int idParlor;
    private int idActor;



    public User() {
    }
    public User(String name, String email, String password) {
        this.name = name;
        this.email = email;
        this.password = password;
    }

    public User(String name, ActorRole role, int idParlor, int idActor) {
        this.name = name;
        this.role = role;
        this.idParlor = idParlor;
        this.idActor = idActor;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ActorRole getRole() {
        return role;
    }

    public void setRole(ActorRole role) {
        this.role = role;
    }

    public void setParlor(Parlor parlor) { this.parlor = parlor; }

    public Parlor getParlor() { return parlor; }

    public boolean isStatus() { return status; }

    public void setStatus(boolean status) { this.status = status; }

    public int getIdParlor() { return idParlor; }

    public void setIdParlor(int idParlor) { this.idParlor = idParlor; }

    public int getIdActor() { return idActor; }

    public void setIdActor(int idActor) { this.idActor = idActor; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof User)) return false;
        User user = (User) o;
        return status == user.status &&
                idParlor == user.idParlor &&
                idActor == user.idActor &&
                Objects.equals(name, user.name) &&
                Objects.equals(email, user.email) &&
                Objects.equals(password, user.password) &&
                role == user.role &&
                Objects.equals(parlor, user.parlor);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, email, password, role, parlor, status, idParlor, idActor);
    }
}
