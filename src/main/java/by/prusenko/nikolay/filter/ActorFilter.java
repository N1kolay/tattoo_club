package by.prusenko.nikolay.filter;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.entity.ActorRole;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * The {@code ActorFilter} class
 * is an implementation of {@code Filter} interface,
 *
 * Filters the jsp actor package -
 * forwards request and response to the index page if a actor role is user or parlor.
 *
 * @author Nikolay Prusenko
 */
@WebFilter(urlPatterns = {"/jsp/actor/*"})
public class ActorFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) request;
        HttpServletResponse httpServletResponse = (HttpServletResponse) response;

        String role = (String) httpServletRequest.getSession().getAttribute("role");
        if (role==null || (!role.equals(ActorRole.USER.toString()) &&
                !role.equals(ActorRole.PARLOR.toString()))) {
            RequestDispatcher dispatcher = httpServletRequest.getServletContext().getRequestDispatcher(Page.INDEX);
            dispatcher.forward(httpServletRequest, httpServletResponse);
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {

    }
}
