package by.prusenko.nikolay.mail;

import by.prusenko.nikolay.util.SecurePassword;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Enumeration;
import java.util.Properties;
import java.util.ResourceBundle;

public class MailThread extends Thread {
    private static final Logger LOGGER = LogManager.getLogger(MailThread.class);
    private static final String MAIL_PROPERTY = "mail";
    private static final String MAIL_USER_NAME = "mail.user.name";
    private static final String MAIL_USER_PASSWORD = "mail.user.password";
    private static final String MESSAGE_PATTERN = "<p>Подтвердить регистрацию:<br><br>\n" +
            "<a href=\"http://localhost:8080/TattooClub?command=confirmsignup&name=username&token=userhash\">Подтвердить</a>";
    private static final String subject = "Учетная запись TattooClub — подтверждение адреса электронной почты";
    private String sendToEmail;
    private String message;


    public MailThread(String email,String name) {
        this.sendToEmail = email;
        this.message = MESSAGE_PATTERN.replace("username",name).replace("userhash", SecurePassword.hash(name));
    }


    @Override
    public void run() {
        ResourceBundle resource = ResourceBundle.getBundle(MAIL_PROPERTY);
        Properties properties = convertResourceBundleToProperties(resource);
        String name = resource.getString(MAIL_USER_NAME);
        String password = resource.getString(MAIL_USER_PASSWORD);
        Session session = getInstance(properties,name,password);
        MimeMessage msg = new MimeMessage(session);

        try {
            msg.setFrom(new InternetAddress(name));
            msg.setSubject(subject);
            msg.setContent(message, "text/html;charset=utf-8");
            msg.setRecipient(Message.RecipientType.TO, new InternetAddress(sendToEmail));
        } catch (MessagingException e) {
            LOGGER.log(Level.ERROR, "error of formed message", e);
        }
        try {
            Transport.send(msg);
        } catch (MessagingException e) {
            LOGGER.log(Level.ERROR, "error of sending", e);
        }
    }

    private static Properties convertResourceBundleToProperties(ResourceBundle resource) {
        Properties properties = new Properties();
        Enumeration<String> keys = resource.getKeys();
        while (keys.hasMoreElements()) {
            String key = keys.nextElement();
            properties.put(key, resource.getString(key));
        }
        return properties;
    }

    private static Session getInstance(Properties properties, String NAME, String PASSWORD){
        return Session.getInstance(properties, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(NAME, PASSWORD);
            }
        });
    }
}
