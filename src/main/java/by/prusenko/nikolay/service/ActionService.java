package by.prusenko.nikolay.service;


import by.prusenko.nikolay.exception.ServiceException;

import java.util.Collections;
import java.util.List;
import java.util.Map;


public abstract class ActionService {
    public abstract String processAction(Map<String, String> parameters) throws ServiceException;
    /**
     * Returns a map that has a {@code String} name of attribute as key and {@code String} attribute as value.
     * Each command should have own implementation.
     *
     * @return a map - contains {@code String} attributes as value.
     */
    public Map<String, String> receiveAttributes() {
        return Collections.emptyMap();
    }
    /**
     * Returns a map that has a {@code String} name of attribute as key and {@code List} attribute as value.
     * Each command should have own implementation.
     *
     * @return a map - contains {@code List} attributes as value.
     */
    public Map<String, List> receiveListAttributes() {
        return Collections.emptyMap();
    }
    /**
     * Returns a map that has a {@code String} name as key and {@code String} parameter as value.
     * Each command should have own implementation.
     *
     * @return map - contains {@code String} message as value.
     */
    public Map<String, String> receiveMessage() {
        return Collections.emptyMap();
    }
}
