package by.prusenko.nikolay.service.impl.base;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.UserDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;
import org.mindrot.jbcrypt.BCrypt;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.ACTOR_NAME;
import static by.prusenko.nikolay.constant.Attribute.TOKEN;

/**
 * The {@code ConfirmSignUpService} class
 * is a service class used to connect command
 * with UserDao.
 *
 * @author Nikolay Prusenko
 */
public class ConfirmSignUpService extends ActionService {
    private UserDao userDao;

    public ConfirmSignUpService(UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * Returns a path, updates actor's status if received token is correct
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        String name = parameters.get(ACTOR_NAME);
        String token = parameters.get(TOKEN);
        if (BCrypt.checkpw(name, token)) {
            try {
                userDao.updateStatus(name);
            } catch (DaoException e) {
                throw new ServiceException(e.getMessage(), e);
            }
        }
        return Page.MAIN;
    }
}
