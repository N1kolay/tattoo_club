package by.prusenko.nikolay.service.impl.base;

import by.prusenko.nikolay.service.ActionService;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code LanguageService} class
 * is a service class used to change locale.
 *
 * @author Nikolay Prusenko
 */
public class LanguageService extends ActionService {
    private Map<String, String> message;
    /**
     * Returns a path, changes a locale.
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     */
    @Override
    public String processAction(Map<String, String> parameters) {
        String locale = parameters.get(LANG);
        String page = parameters.get(PATH);
        message = Map.of(LOCALE, locale);
        return page;
    }
    /**
     * Returns a map that has a {@code String} name as key and {@code String} parameter as value.
     *
     * @return map - contains {@code String} message as value.
     */
    @Override
    public Map<String, String> receiveMessage() {
        return message;
    }
}
