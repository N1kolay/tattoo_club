package by.prusenko.nikolay.service.impl.base;


import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.UserDao;
import by.prusenko.nikolay.entity.User;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;

import java.util.Map;
import java.util.Optional;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code LoginService} class
 * is a service class used to connect command
 * with UserDao.
 *
 * @author Nikolay Prusenko
 */
public class LoginService extends ActionService {
    private UserDao userDao;
    private Map<String, String> attributes;

    public LoginService(UserDao userDao) {
        this.userDao = userDao;
    }

    /**
     * Returns a path, checks if an actor is in database
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        String email = parameters.get(ACTOR_EMAIL);
        String pass = parameters.get(ACTOR_PASSWORD);
        String page = Page.MAIN;
        try {
            Optional<User> optionalUser = userDao.findUser(email, pass);
            if (optionalUser.isPresent()) {
                User user = optionalUser.get();
                attributes = Map.of(ACTOR_NAME, user.getName(), ACTOR_ROLE, user.getRole().name().toLowerCase(),
                        ID_PARLOR, String.valueOf(user.getIdParlor()), ID_ACTOR, String.valueOf(user.getIdActor()));
            } else {
                page = Page.LOGIN + "?message=error";
            }
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return page;
    }
    /**
     * Returns a map that has a {@code String} name of attribute as key and {@code String} attribute as value.
     * Checks if an actor is in database
     *
     * @return a map - contains {@code String} attributes as value.
     */
    @Override
    public Map<String, String> receiveAttributes() {
        return attributes;
    }
}
