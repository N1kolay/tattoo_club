package by.prusenko.nikolay.service.impl.base;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.UserDao;
import by.prusenko.nikolay.entity.User;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.mail.MailThread;
import by.prusenko.nikolay.service.ActionService;
import by.prusenko.nikolay.util.MessageManager;
import by.prusenko.nikolay.util.SecurePassword;
import by.prusenko.nikolay.util.InputValidator;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code SignUpService} class
 * is a service class used to connect command
 * with UserDao.
 *
 * @author Nikolay Prusenko
 */
public class SignUpService extends ActionService {
    private UserDao userDao;
    private Map<String, String> message;

    public SignUpService(UserDao userDao) {
        this.userDao = userDao;
    }
    /**
     * Returns a path, adds new actor in database if all parameters are correct
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        String name = parameters.get(ACTOR_NAME);
        String email = parameters.get(ACTOR_EMAIL);
        String pass = parameters.get(ACTOR_PASSWORD);
        String passConfirm = parameters.get(ACTOR_PASSWORD_CONFIRMATION);
        String page;

        if (InputValidator.validInput(name, email, pass, passConfirm)) {
            try {
                if (checkNameEmail(name, email) && addUser(name, email, pass)) {
                    new MailThread(email, name).start();
                    message = Map.of("message", MessageManager.getProperty("message.nameEmailCheck", LOCALE));
                    page = Page.LOGIN;
                } else {
                    message = Map.of("message", MessageManager.getProperty("message.nameEmailError", LOCALE));
                    page = Page.SIGN_UP;
                }
            } catch (DaoException e) {
                throw new ServiceException(e.getMessage(), e);
            }
        } else {
            String msg = InputValidator.receiveMessage();
            message = Map.of("message", msg);
            page = Page.SIGN_UP;
        }
        return page;
    }

    /**
     * Returns boolean result, adds an actor in database
     *
     * @param name {@code String}
     * @param email {@code String}
     * @param pass {@code String}
     *
     * @return {@code true} if user is added. {@code false} if is not.
     * @throws DaoException if database access error occurs
     */
    private boolean addUser(String name, String email, String pass) throws DaoException {
        User user = new User(name, email, pass);
        user.setPassword(SecurePassword.hash(user.getPassword()));
        return userDao.addUser(user);
    }
    /**
     * Returns boolean result, checks an actor in database for uniqueness
     *
     * @param name {@code String}
     * @param email {@code String}
     *
     * @return {@code true} if user is unique. {@code false} if is not.
     * @throws DaoException if database access error occurs
     */
    private boolean checkNameEmail(String name, String email) throws DaoException {
        return userDao.uniqueNameEmail(name, email);
    }
    /**
     * Returns a map that has a {@code String} name as key and {@code String} parameter as value.
     *
     * @return map - contains {@code String} message as value.
     */
    @Override
    public Map<String, String> receiveMessage() {
        return message;
    }
}
