package by.prusenko.nikolay.service.impl.feedback;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.FeedbackDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code SignUpService} class
 * is a service class used to connect command
 * with FeedbackDao.
 *
 * @author Nikolay Prusenko
 */
public class AddAnswerService extends ActionService {
    private FeedbackDao feedbackDao;

    public AddAnswerService(FeedbackDao feedbackDao) {
        this.feedbackDao = feedbackDao;
    }
    /**
     * Returns a path, adds answer (to feedback) in database
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {

        String answer = parameters.get(ANSWER);
        int idFeedback = Integer.parseInt(parameters.get(ID_FEEDBACK));
        int idActor = Integer.parseInt(parameters.get(ID_ACTOR));
        int idParlor = Integer.parseInt(parameters.get(ID_PARLOR));

        try {
            feedbackDao.addAnswer(answer, idFeedback, idActor);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return Page.FEEDBACK_COMMAND + idParlor;
    }
}

