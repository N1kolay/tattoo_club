package by.prusenko.nikolay.service.impl.feedback;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.FeedbackDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code DeleteFeedbackService} class
 * is a service class used to connect command
 * with FeedbackDao.
 *
 * @author Nikolay Prusenko
 */
public class DeleteFeedbackService extends ActionService {
    private FeedbackDao feedbackDao;

    public DeleteFeedbackService(FeedbackDao feedbackDao) {
        this.feedbackDao = feedbackDao;
    }
    /**
     * Returns a path, deletes feedback from database
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        int id = Integer.parseInt(parameters.get(ID_FEEDBACK));
        String idParlor = parameters.get(ID_PARLOR);

        try {
            if (parameters.containsKey(ANSWER)) {
                feedbackDao.deleteAnswerFeedback(id);
            } else {
                feedbackDao.deleteFeedback(id);
            }
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return Page.FEEDBACK_COMMAND + idParlor;
    }
}

