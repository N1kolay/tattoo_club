package by.prusenko.nikolay.service.impl.feedback;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.FeedbackDao;
import by.prusenko.nikolay.entity.Feedback;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.FEEDBACK;
import static by.prusenko.nikolay.constant.Attribute.ID_PARLOR;

/**
 * The {@code FeedbackService} class
 * is a service class used to connect command
 * with FeedbackDao.
 *
 * @author Nikolay Prusenko
 */
public class FeedbackService extends ActionService {
    private FeedbackDao feedbackDao;
    private Map<String,String> attribute;
    private Map<String,List> attributeList;

    public FeedbackService(FeedbackDao feedbackDao) {
        this.feedbackDao = feedbackDao;
    }
    /**
     * Returns a path, finds all parlor's feedback in database
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        try {
            int idParlor = Integer.parseInt(parameters.get(ID_PARLOR));
            attribute= Map.of(ID_PARLOR,String.valueOf(idParlor));
            List<Feedback> feedbackList = feedbackDao.findFeedbackByParlor(idParlor);
            attributeList = Map.of(FEEDBACK, feedbackList);

        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return Page.FEEDBACK;
    }
    /**
     * Returns a map that has a {@code String} name of attribute as key and {@code String} attribute as value.
     * Checks if an actor is in database
     *
     * @return a map - contains {@code String} attributes as value.
     */
    @Override
    public Map<String, String> receiveAttributes() {
        return attribute;
    }
    /**
     * Returns a map that has a {@code String} name of attribute as key and {@code List} attribute as value.
     * Checks if an actor is in database
     *
     * @return a map - contains {@code List} attributes as value.
     */
    @Override
    public Map<String, List> receiveListAttributes() {
        return attributeList;
    }
}
