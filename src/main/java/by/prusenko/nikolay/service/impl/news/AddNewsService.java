package by.prusenko.nikolay.service.impl.news;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.constant.SQLConstant;
import by.prusenko.nikolay.dao.NewsDao;
import by.prusenko.nikolay.entity.ActorRole;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;
import by.prusenko.nikolay.util.MessageManager;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code FeedbackService} class
 * is a service class used to connect command
 * with  NewsDao.
 *
 * @author Nikolay Prusenko
 */
public class AddNewsService extends ActionService {
    private NewsDao newsDao;
    private Map<String,String> message;

    public AddNewsService(NewsDao newsDao) {
        this.newsDao = newsDao;
    }
    /**
     * Returns a path, adds news in database if all parameters are correct
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        String type = parameters.get(NEWS_TYPE);
        String title = parameters.get(NEWS_TITLE);
        String text = parameters.get(TEXT);
        String role = parameters.get(ACTOR_ROLE);
        int idParlor;
        int idActor;
        try {
            if (role.equals(ActorRole.PARLOR.toString())) {
                idParlor = Integer.parseInt(parameters.get(ID_PARLOR));
                newsDao.addNews(SQLConstant.INSERT_INTO_NEWS_PARLOR, title, type, text, idParlor);
            } else if (role.equals(ActorRole.USER.toString()) || role.equals(ActorRole.MODERATOR.toString())) {
                idActor = Integer.parseInt(parameters.get(ID_ACTOR));
                newsDao.addNews(SQLConstant.INSERT_INTO_NEWS_ACTOR, title, type, text, idActor);
            }
            message=Map.of("addNewsMessage", MessageManager.getProperty("message.News", LOCALE));
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return Page.ADD_NEWS;
    }
    /**
     * Returns a map that has a {@code String} name as key and {@code String} parameter as value.
     *
     * @return map - contains {@code String} message as value.
     */
    @Override
    public Map<String, String> receiveMessage() {
        return message;
    }
}
