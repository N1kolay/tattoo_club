package by.prusenko.nikolay.service.impl.news;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.NewsDao;
import by.prusenko.nikolay.entity.News;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code EditAllNewsService} class
 * is a service class used to connect command
 * with  NewsDao.
 *
 * @author Nikolay Prusenko
 */
public class EditAllNewsService extends ActionService {
    private NewsDao newsDao;
    public EditAllNewsService(NewsDao newsDao) {
        this.newsDao = newsDao;
    }
    /**
     * Returns a path, edits/deletes posted news
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        String newsId = parameters.get(ID_NEWS);
        String type = parameters.get(NEWS_TYPE);
        try {
            if (parameters.get(BUTTON).equals(EDIT)) {
                newsDao.inactivateNews(Integer.parseInt(newsId));
            } else {
                newsDao.deleteNews(Integer.parseInt(newsId));
            }
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(),e);
        }
        return Page.COMMAND+type;
}

}
