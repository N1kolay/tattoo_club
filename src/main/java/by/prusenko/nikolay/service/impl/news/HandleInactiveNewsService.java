package by.prusenko.nikolay.service.impl.news;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.constant.SQLConstant;
import by.prusenko.nikolay.dao.NewsDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code HandleInactiveNewsService} class
 * is a service class used to connect command
 * with  NewsDao.
 *
 * @author Nikolay Prusenko
 */
public class HandleInactiveNewsService extends ActionService {
    private NewsDao newsDao;

    public HandleInactiveNewsService(NewsDao newsDao) {
        this.newsDao = newsDao;
    }
    /**
     * Returns a path, edits/deletes not yet posted news
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        String type = parameters.get(NEWS_TYPE);
        int id = Integer.parseInt(parameters.get(ID_NEWS));
        try {
            if (parameters.get(BUTTON).equals(ADD)) {
                String title = parameters.get(NEWS_TITLE);
                String text = parameters.get(TEXT);
                newsDao.addNews(SQLConstant.UPDATE_NEWS_TO_ACTIVE, title, type, text, id);
            } else {
                newsDao.deleteNews(id);
            }
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return Page.INACTIVE_NEWS_COMMAND;
    }
}
