package by.prusenko.nikolay.service.impl.news;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.NewsDao;
import by.prusenko.nikolay.entity.News;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;

import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code InactiveNewsService} class
 * is a service class used to connect command
 * with  NewsDao.
 *
 * @author Nikolay Prusenko
 */
public class InactiveNewsService extends ActionService {
    private NewsDao newsDao;
    private static final String INACTIVE_NEWS = "inactiveNews";
    private Map<String,List> attributeList;
    public InactiveNewsService(NewsDao newsDao) {
        this.newsDao = newsDao;
    }

    /**
     * Returns a path, finds all not posted news (inactive) in database
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String,String> parameters) throws ServiceException {
        try {
            List<News> list = newsDao.findInactiveNews(parameters.get(STATUS));
            attributeList = Map.of(INACTIVE_NEWS, list);
        } catch (DaoException e) {
              throw new ServiceException(e.getMessage(),e);
        }
        return Page.INACTIVE_NEWS;
    }
    /**
     * Returns a map that has a {@code String} name of attribute as key and {@code List} attribute as value.
     * Checks if an actor is in database
     *
     * @return a map - contains {@code List} attributes as value.
     */
    @Override
    public Map<String, List> receiveListAttributes() {
        return attributeList;
    }
}
