package by.prusenko.nikolay.service.impl.news;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.NewsDao;
import by.prusenko.nikolay.entity.News;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;

import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code NewsService} class
 * is a service class used to connect command
 * with  NewsDao.
 *
 * @author Nikolay Prusenko
 */
public class NewsService extends ActionService {
    private static final String LIST_OF_NEWS = "list";
    private NewsDao newsDao;
    private Map<String,List> attribute;

    public NewsService(NewsDao newsDao) {
        this.newsDao = newsDao;
    }
    /**
     * Returns a path, finds all posted news
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        String type = parameters.get(NEWS_TYPE);
        String page;
        try {
            List<News> list = newsDao.findActiveNews(type);
            attribute = Map.of(LIST_OF_NEWS, list);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        switch (type) {
            case ("news"):
                page = Page.NEWS;
                break;
            case ("article"):
                page = Page.ARTICLES;
                break;
            case ("discount"):
                page = Page.DISCOUNTS;
                break;
            default:
                page = Page.MAIN;
        }
        return page;
    }
    /**
     * Returns a map that has a {@code String} name of attribute as key and {@code List} attribute as value.
     * Checks if an actor is in database
     *
     * @return a map - contains {@code List} attributes as value.
     */
    @Override
    public Map<String, List> receiveListAttributes() {
        return attribute;
    }
}
