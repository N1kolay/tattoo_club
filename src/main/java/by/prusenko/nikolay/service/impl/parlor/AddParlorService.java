package by.prusenko.nikolay.service.impl.parlor;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code AddParlorService} class
 * is a service class used to connect command
 * with  ParlorDao.
 *
 * @author Nikolay Prusenko
 */
public class AddParlorService extends ActionService {
    private ParlorDao parlorDao;
    private Map<String, String> attribute;

    public AddParlorService(ParlorDao parlorDao) {
        this.parlorDao = parlorDao;
    }
    /**
     * Returns a path, adds new parlor in database
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        String parlor = parameters.get(PARLOR_NAME);
        String country = parameters.get(PARLOR_COUNTRY);
        String city = parameters.get(PARLOR_CITY);
        String address = parameters.get(PARLOR_ADDRESS);
        String name = parameters.get(ACTOR_NAME);

        try {
            parlorDao.chainParlorUser(name, parlor, country, city, address);
            int id = parlorDao.findParlorByAddress(parlor, address);
            attribute = Map.of(ID_PARLOR, String.valueOf(id));
            return Page.PROFILE;
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
    }
    /**
     * Returns a map that has a {@code String} name of attribute as key and {@code String} attribute as value.
     * Checks if an actor is in database
     *
     * @return a map - contains {@code String} attributes as value.
     */
    @Override
    public Map<String, String> receiveAttributes() {
        return attribute;
    }
}
