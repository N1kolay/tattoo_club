package by.prusenko.nikolay.service.impl.parlor;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.entity.Parlor;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;

import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.STATUS;

/**
 * The {@code AllParlorsService} class
 * is a service class used to connect command
 * with  ParlorDao.
 *
 * @author Nikolay Prusenko
 */
public class AllParlorsService extends ActionService {
    private static final String LIST_OF_PARLORS = "list";
    private ParlorDao parlorDao;
    private Map<String, List> attribute;

    public AllParlorsService(ParlorDao parlorDao) {
        this.parlorDao = parlorDao;
    }
    /**
     * Returns a path, finds all registered (active) parlors
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        String status = parameters.get(STATUS);
        try {
            List<Parlor> listParlor = parlorDao.findActiveParlor(status);
            attribute = Map.of(LIST_OF_PARLORS, listParlor);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return Page.PARLORS;
    }
    /**
     * Returns a map that has a {@code String} name of attribute as key and {@code List} attribute as value.
     * Checks if an actor is in database
     *
     * @return a map - contains {@code List} attributes as value.
     */
    @Override
    public Map<String, List> receiveListAttributes() {
        return attribute;
    }
}
