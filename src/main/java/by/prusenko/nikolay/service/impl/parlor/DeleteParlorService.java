package by.prusenko.nikolay.service.impl.parlor;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.entity.ActorRole;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;
import by.prusenko.nikolay.util.MessageManager;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code DeleteParlorService} class
 * is a service class used to connect command
 * with  ParlorDao.
 *
 * @author Nikolay Prusenko
 */
public class DeleteParlorService extends ActionService {
    private ParlorDao parlorDao;
    private Map<String, String> attribute;
    private Map<String, String> message;

    public DeleteParlorService(ParlorDao parlorDao) {
        this.parlorDao = parlorDao;
    }
    /**
     * Returns a path, deletes parlor from database
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        int idActor = Integer.parseInt(parameters.get(ID_ACTOR));
        try {
            parlorDao.deleteParlorFromUser(idActor);
            attribute = Map.of(ACTOR_ROLE, ActorRole.USER.toString(), ID_PARLOR, "0");
            message = Map.of("deleteParlor",
                    MessageManager.getProperty("message.deleteParlor", LOCALE));
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return Page.PROFILE;
    }
    /**
     * Returns a map that has a {@code String} name of attribute as key and {@code String} attribute as value.
     * Checks if an actor is in database
     *
     * @return a map - contains {@code String} attributes as value.
     */
    @Override
    public Map<String, String> receiveAttributes() {
        return attribute;
    }
    /**
     * Returns a map that has a {@code String} name as key and {@code String} parameter as value.
     *
     * @return map - contains {@code String} message as value.
     */
    @Override
    public Map<String, String> receiveMessage() {
        return message;
    }

}
