package by.prusenko.nikolay.service.impl.parlor;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;

/**
 * The {@code HandleInactiveParlorService} class
 * is a service class used to connect command
 * with  ParlorDao.
 *
 * @author Nikolay Prusenko
 */
public class HandleInactiveParlorService extends ActionService {
    private ParlorDao parlorDao;

    public HandleInactiveParlorService(ParlorDao parlorDao) {
        this.parlorDao = parlorDao;
    }
    /**
     * Returns a path, registers/deletes added parlors
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        String idParlor = parameters.get(ID_PARLOR);
        String button = parameters.get(BUTTON);

        try {
            if (button.equals(ADD)) {
                parlorDao.activateParlor(Integer.parseInt(idParlor));
            } else {
                parlorDao.deleteIncorrectParlor(Integer.parseInt(idParlor));
            }
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return Page.INACTIVE_PARLOR_COMMAND;
    }
}
