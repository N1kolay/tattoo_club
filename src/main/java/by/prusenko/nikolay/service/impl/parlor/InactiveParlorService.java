package by.prusenko.nikolay.service.impl.parlor;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.entity.User;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.ActionService;

import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.STATUS;

/**
 * The {@code InactiveParlorService} class
 * is a service class used to connect command
 * with  ParlorDao.
 *
 * @author Nikolay Prusenko
 */
public class InactiveParlorService extends ActionService {
    private ParlorDao parlorDao;
    private static final String UNFIX_PARLOR = "unfixParlor";
    private Map<String, List> attributeList;

    public InactiveParlorService(ParlorDao parlorDao) {
        this.parlorDao = parlorDao;
    }
    /**
     * Returns a path, finds all unregistered(inactive) parlors
     *
     * @param parameters a map of parameters
     * @return a created path {@code String}
     * @throws ServiceException if {@code DaoException} occurs (database access error)
     */
    @Override
    public String processAction(Map<String, String> parameters) throws ServiceException {
        try {
            List<User> list = parlorDao.findInactiveParlor(parameters.get(STATUS));
            attributeList = Map.of(UNFIX_PARLOR, list);
        } catch (DaoException e) {
            throw new ServiceException(e.getMessage(), e);
        }
        return Page.INACTIVE_PARLOR;
    }
    /**
     * Returns a map that has a {@code String} name of attribute as key and {@code List} attribute as value.
     * Checks if an actor is in database
     *
     * @return a map - contains {@code List} attributes as value.
     */
    @Override
    public Map<String, List> receiveListAttributes() {
        return attributeList;
    }
}
