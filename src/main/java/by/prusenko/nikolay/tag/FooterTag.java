package by.prusenko.nikolay.tag;

import by.prusenko.nikolay.util.MessageManager;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.Locale;

import static by.prusenko.nikolay.constant.Attribute.*;


public class FooterTag extends TagSupport {
    @Override
    public int doStartTag() throws JspException {
        String name = (String) pageContext.getSession().getAttribute(ACTOR_NAME);
        String role = (String) pageContext.getSession().getAttribute(ACTOR_ROLE);
        String locale = (String) pageContext.getSession().getAttribute(LOCALE);
        if (locale == null) {
            locale = Locale.getDefault().toString();
        }
        String loggedFirst = MessageManager.getProperty("page.tag.loggedFirst", locale);
        String loggedSecond = MessageManager.getProperty("page.tag.loggedSecond", locale);
        String notLoggedIn = MessageManager.getProperty("page.tag.notLoggedIn", locale);
        String info;

        if (role != null) {
            info = loggedFirst + role + loggedSecond + name;
        } else {
            info = notLoggedIn;
        }

        JspWriter out = pageContext.getOut();
        try {
            out.write(info);
        } catch (IOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
