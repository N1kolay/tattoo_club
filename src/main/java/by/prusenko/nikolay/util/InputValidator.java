package by.prusenko.nikolay.util;

import org.apache.commons.validator.routines.EmailValidator;

import java.util.regex.Pattern;

import static by.prusenko.nikolay.constant.Attribute.LOCALE;

public class InputValidator {
    private static final String NAME_REGEXP = "[А-ЯA-Z][a-яa-z]{1,20}";
    private static final String EMAIL_REGEXP =  "[a-z0-9._%+-]{1,20}@[a-z0-9.-]{1,16}\\.[a-z]{2,3}";
    private static final String PASSWORD_REGEXP =  "(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}";


    private static String message;

    private InputValidator() {
    }

    public static boolean validInput(String name, String email, String pass, String repPass) {
        if (!Pattern.matches(NAME_REGEXP, name)) {
            message =  MessageManager.getProperty("message.nameError", LOCALE);
            return false;
        }
        if(!Pattern.matches(EMAIL_REGEXP,email)){
         // if(!EmailValidator.getInstance().isValid(email)){
            message = MessageManager.getProperty("message.emailError", LOCALE);
            return false;
        }
        if (!Pattern.matches(PASSWORD_REGEXP, pass)) {
            message = MessageManager.getProperty("message.passError", LOCALE);
            return false;
        }
        if (!validRepeatPassword(pass, repPass)) {
            message = MessageManager.getProperty("message.repError", LOCALE);
            return false;
        }
        return true;
    }

    public static String receiveMessage() {
        return message;
    }


    private static boolean validRepeatPassword(String password, String repPassword) {
        return password.equals(repPassword);
    }

    public static boolean validateName(String name){
        return Pattern.matches(NAME_REGEXP,name);
    }
    public static boolean validateEmail(String email){
        return EmailValidator.getInstance().isValid(email);
    }
}
