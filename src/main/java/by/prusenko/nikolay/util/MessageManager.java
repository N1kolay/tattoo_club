package by.prusenko.nikolay.util;

import java.util.Locale;
import java.util.ResourceBundle;

public class MessageManager {
    private final static String RU_LOCALE = "ru_RU";
    private final static String ENG_LOCALE = "eng_ENG";
    private final static String DEFAULT_LOCALE = "content";

    private MessageManager() {

    }

    public static String getProperty(String key, String language) {
        ResourceBundle resourceBundle;
        Locale locale;
        if (language != null) {
            switch (language) {
                case RU_LOCALE:
                    locale = new Locale("ru", "RU");
                    resourceBundle = ResourceBundle.getBundle(DEFAULT_LOCALE, locale);
                    break;
                case ENG_LOCALE:
                    locale = new Locale("eng", "ENG");
                    resourceBundle = ResourceBundle.getBundle(DEFAULT_LOCALE, locale);
                    break;
                default:
                    resourceBundle = ResourceBundle.getBundle(DEFAULT_LOCALE);
                    break;
            }
        }
        else {
            resourceBundle = ResourceBundle.getBundle(DEFAULT_LOCALE);
        }
        return resourceBundle.getString(key);
    }


}
