package by.prusenko.nikolay.util;

import java.util.Objects;

public class Router {
    private String path;
    private RouterType type;

    public Router() {

    }

    public Router(String path, RouterType type) {
        this.path = path;
        this.type = type;
    }

    public enum RouterType{
        FORWARD,REDIRECT,
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public RouterType getType() {
        return type;
    }

    public void setType(RouterType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Router)) return false;
        Router router = (Router) o;
        return Objects.equals(path, router.path) &&
                type == router.type;
    }

    @Override
    public int hashCode() {

        return Objects.hash(path, type);
    }
}
