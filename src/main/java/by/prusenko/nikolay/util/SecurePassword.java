package by.prusenko.nikolay.util;

import org.mindrot.jbcrypt.BCrypt;

public class SecurePassword {

    private static final int WORKLOAD = 12;

    private SecurePassword() {
    }

    public static String hash(String plain_password){
        String salt = BCrypt.gensalt(WORKLOAD);
        return BCrypt.hashpw(plain_password, salt);
    }
}
