package by.prusenko.nikolay.command.factory;

import by.prusenko.nikolay.command.SimpleCommand;
import by.prusenko.nikolay.command.impl.base.EmptyCommand;
import by.prusenko.nikolay.command.impl.feedback.FeedbackCommand;
import by.prusenko.nikolay.service.impl.feedback.FeedbackService;
import org.junit.Assert;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class ActionFactoryTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private FeedbackService feedbackService;


    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
    }
    @Test
    public void shouldSuccessfullyDefineCommand() {
        // given
        given(request.getParameter("command")).willReturn("feedback");
        SimpleCommand expectedCommand = new FeedbackCommand(feedbackService);
        // when
        SimpleCommand actualCommand = new ActionFactory().defineCommand(request);
        // then
        Assert.assertEquals(actualCommand.getClass(), expectedCommand.getClass());
    }
    @Test
    public void shouldSuccessfullyDefineEmptyCommand() {
        // given
        given(request.getParameter("command")).willReturn("");
        SimpleCommand expectedCommand = new EmptyCommand();
        // when
        SimpleCommand actualCommand = new ActionFactory().defineCommand(request);
        // then
        Assert.assertEquals(actualCommand.getClass(), expectedCommand.getClass());
    }
}