package by.prusenko.nikolay.command.impl.base;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.base.ConfirmSignUpService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.ACTOR_NAME;
import static by.prusenko.nikolay.constant.Attribute.TOKEN;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class ConfirmSignUpCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private ConfirmSignUpService confirmSignUpService;

    private BaseCommand confirmSignUpCommand;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        confirmSignUpCommand = new ConfirmSignUpCommand(confirmSignUpService);
    }

    @Test
    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(ACTOR_NAME)).willReturn("testName");
        given(request.getParameter(TOKEN)).willReturn("testToken");
        given(request.getSession(true)).willReturn(session);
        Map<String, String> testParameters = Map.of(ACTOR_NAME, "testName", TOKEN, "testToken");
        given(confirmSignUpService.processAction(testParameters)).willReturn("testPath");
        given(confirmSignUpService.receiveAttributes()).willReturn(Collections.emptyMap());
        Router expectedRouter = new Router("testPath",Router.RouterType.FORWARD);
        // when
        Router actualRouter = confirmSignUpCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);
    }

}