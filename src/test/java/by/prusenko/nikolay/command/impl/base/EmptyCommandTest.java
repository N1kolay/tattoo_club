package by.prusenko.nikolay.command.impl.base;

import by.prusenko.nikolay.command.SimpleCommand;
import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;

import static org.mockito.MockitoAnnotations.initMocks;


@Test
public class EmptyCommandTest {
    @Mock
    private HttpServletRequest request;

    private SimpleCommand command;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        command = new EmptyCommand();
    }

    @Test
    public void shouldSuccessfullyRetrieveRouter() {
        // given
        Router actualRouter = new Router(Page.MAIN, Router.RouterType.REDIRECT);
        // when
        Router expectedRouter = command.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);
    }

    @Test
    public void shouldUnsuccessfullyRetrieveRouter() {
        // given
        Router actualRouter = new Router(Page.MESSAGE, Router.RouterType.REDIRECT);
        // when
        Router expectedRouter = command.execute(request);
        // then
        Assert.assertNotEquals(actualRouter, expectedRouter);
    }

}