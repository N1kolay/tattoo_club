package by.prusenko.nikolay.command.impl.base;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.service.impl.base.LanguageService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.LANG;
import static by.prusenko.nikolay.constant.Attribute.PATH;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class LanguageCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private LanguageService confirmSignUpService;

    private BaseCommand languageCommand;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        languageCommand = new LanguageCommand(confirmSignUpService);
    }

    @Test
    public void shouldSuccessfullyRetrieveRouter() {
        // given
        given(request.getSession()).willReturn(session);
        given(request.getParameter(LANG)).willReturn("ru_RU");
        given(session.getAttribute(PATH)).willReturn("/testPath.jsp");
        given(request.getServletPath()).willReturn("");
        Map<String, String> testParameters = Map.of(LANG, "ru_RU", PATH, "/testPath.jsp");
        given(confirmSignUpService.processAction(testParameters)).willReturn("testPath");
        given(confirmSignUpService.receiveAttributes()).willReturn(Collections.emptyMap());
        Router expectedRouter = new Router("testPath", Router.RouterType.FORWARD);
        // when
        Router actualRouter = languageCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);
    }

}