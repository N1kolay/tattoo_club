package by.prusenko.nikolay.command.impl.base;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.base.LoginService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class LoginCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private LoginService loginService;

    private BaseCommand loginCommand;

    @BeforeMethod
    public void setUp() {
        initMocks(this);
        loginCommand = new LoginCommand(loginService);
    }

    @Test
    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(ACTOR_EMAIL)).willReturn("testEmail");
        given(request.getParameter(ACTOR_PASSWORD)).willReturn("testPassword");
        given(request.getSession(true)).willReturn(session);
        Map<String, String> testParameters = Map.of(ACTOR_EMAIL, "testEmail", ACTOR_PASSWORD, "testPassword");
        given(loginService.processAction(testParameters)).willReturn("testPath");
        given(loginService.receiveAttributes()).willReturn(Collections.emptyMap());
        Router expectedRouter = new Router("testPath",Router.RouterType.REDIRECT);
        // when
        Router actualRouter = loginCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);
    }


}