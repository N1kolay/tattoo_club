package by.prusenko.nikolay.command.impl.base;

import by.prusenko.nikolay.command.SimpleCommand;
import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;


@Test
public class LogoutCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;

    private SimpleCommand logoutCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        logoutCommand = new LogoutCommand();
    }

    @Test
    public void shouldSuccessfullyRetrieveRouter() {
        // given
        given(request.getSession()).willReturn(session);
        Router expectedRouter = new Router(Page.MAIN, Router.RouterType.REDIRECT);
        // when
        Router actualRouter = logoutCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);
    }

    @Test
    public void shouldUnsuccessfullyRetrieveRouter() {
        // given
        given(request.getSession()).willReturn(session);
        Router expectedRouter = new Router(Page.LOGIN, Router.RouterType.REDIRECT);
        // when
        Router actualRouter = logoutCommand.execute(request);
        // then
        Assert.assertNotEquals(actualRouter, expectedRouter);
    }
}