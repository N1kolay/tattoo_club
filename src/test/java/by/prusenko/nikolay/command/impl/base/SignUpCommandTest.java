package by.prusenko.nikolay.command.impl.base;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.base.SignUpService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

public class SignUpCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private SignUpService signUpService;

    private BaseCommand signUpCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        signUpCommand = new SignUpCommand(signUpService);
    }

    @Test
    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(ACTOR_NAME)).willReturn("testName");
        given(request.getParameter(ACTOR_EMAIL)).willReturn("testEmail");
        given(request.getParameter(ACTOR_PASSWORD)).willReturn("testPassword");
        given(request.getParameter(ACTOR_PASSWORD_CONFIRMATION)).willReturn("testPassConfirm");
        Map<String, String> testParameters = Map.of(ACTOR_NAME, "testName", ACTOR_EMAIL, "testEmail",
                ACTOR_PASSWORD, "testPassword", ACTOR_PASSWORD_CONFIRMATION, "testPassConfirm");
        given(signUpService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.FORWARD);
        // when
        Router actualRouter = signUpCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);
    }

    @Test
    public void shouldUnsuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(ACTOR_NAME)).willReturn("testName");
        given(request.getParameter(ACTOR_EMAIL)).willReturn("testEmail");
        given(request.getParameter(ACTOR_PASSWORD)).willReturn("testPassword");
        given(request.getParameter(ACTOR_PASSWORD_CONFIRMATION)).willReturn("testPassConfirm");
        Map<String, String> testParameters = Map.of(ACTOR_NAME, "testName", ACTOR_EMAIL, "testEmail",
                ACTOR_PASSWORD, "testPassword", ACTOR_PASSWORD_CONFIRMATION, "testPassConfirm");
        given(signUpService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.REDIRECT);
        // when
        Router actualRouter = signUpCommand.execute(request);
        // then
        Assert.assertNotEquals(actualRouter, expectedRouter);
    }

}