package by.prusenko.nikolay.command.impl.feedback;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.feedback.AddAnswerService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class AddAnswerCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private AddAnswerService addAnswerService;

    private BaseCommand addAnswerCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        addAnswerCommand = new AddAnswerCommand(addAnswerService);
    }

    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(ANSWER)).willReturn("testAnswer");
        given(request.getParameter(ID_FEEDBACK)).willReturn("testIdFeedback");
        given(request.getSession()).willReturn(session);
        given(session.getAttribute(ID_ACTOR)).willReturn("testIdActor");
        given(session.getAttribute(ID_PARLOR)).willReturn("testIdParlor");
        Map<String, String> testParameters = Map.of(ANSWER, "testAnswer", ID_FEEDBACK, "testIdFeedback",
                ID_ACTOR, "testIdActor", ID_PARLOR, "testIdParlor");
        given(addAnswerService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.REDIRECT);
        // when
        Router actualRouter = addAnswerCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter,expectedRouter);
    }

}