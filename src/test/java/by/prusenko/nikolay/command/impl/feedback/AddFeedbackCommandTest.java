package by.prusenko.nikolay.command.impl.feedback;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.feedback.AddFeedbackService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class AddFeedbackCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private AddFeedbackService addFeedbackService;

    private BaseCommand addFeedbackCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        addFeedbackCommand = new AddFeedbackCommand(addFeedbackService);
    }

    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getSession()).willReturn(session);
        given(session.getAttribute(ID_ACTOR)).willReturn("testIdActor");
        given(request.getParameter(RATING)).willReturn("testRating");
        given(request.getParameter(COMMENT)).willReturn("testComment");
        given(request.getParameter(ID_PARLOR)).willReturn("testIdParlor");
        Map<String, String> testParameters = Map.of(ID_ACTOR, "testIdActor", RATING, "testRating", COMMENT,
                "testComment", ID_PARLOR, "testIdParlor");
        given(addFeedbackService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.REDIRECT);
        // when
        Router actualRouter = addFeedbackCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter,expectedRouter);
    }

}