package by.prusenko.nikolay.command.impl.feedback;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.feedback.DeleteFeedbackService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class DeleteFeedbackCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private DeleteFeedbackService deleteFeedbackService;

    private BaseCommand deleteFeedbackCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        deleteFeedbackCommand = new DeleteFeedbackCommand(deleteFeedbackService);
    }

    public void shouldSuccessfullyRetrieveRouterWithAnswer() throws ServiceException {
        // given
        given(request.getParameter(ID_FEEDBACK)).willReturn("testIdFeedback");
        given(request.getParameter(ANSWER)).willReturn("testAnswer");
        given(request.getParameter(ID_PARLOR)).willReturn("testIdParlor");
        Map<String, String> testParameters = Map.of(ID_FEEDBACK, "testIdFeedback", ANSWER, "testAnswer",
                ID_PARLOR, "testIdParlor");
        given(deleteFeedbackService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.REDIRECT);
        // when
        Router actualRouter = deleteFeedbackCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);
    }
}