package by.prusenko.nikolay.command.impl.feedback;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.feedback.FeedbackService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.ID_PARLOR;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class FeedbackCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private FeedbackService feedbackService;

    private BaseCommand feedbackCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        feedbackCommand = new FeedbackCommand(feedbackService);
    }
    @Test
    public void shouldSuccessfullyRetrieveRouterWithAnswer() throws ServiceException {
        // given
        given(request.getParameter(ID_PARLOR)).willReturn("testIdParlor");
        Map<String, String> testParameters = Map.of(ID_PARLOR, "testIdParlor");
        given(feedbackService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.FORWARD);
        // when
        Router actualRouter = feedbackCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter,expectedRouter);
    }
}