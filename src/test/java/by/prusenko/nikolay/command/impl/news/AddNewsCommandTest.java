package by.prusenko.nikolay.command.impl.news;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.news.AddNewsService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class AddNewsCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private AddNewsService addNewsService;

    private BaseCommand addNewsCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        addNewsCommand = new AddNewsCommand(addNewsService);

    }

    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(NEWS_TYPE)).willReturn("testType");
        given(request.getParameter(NEWS_TITLE)).willReturn("testTitle");
        given(request.getParameter(TEXT)).willReturn("testText");
        given(request.getSession()).willReturn(session);
        given(session.getAttribute(ACTOR_ROLE)).willReturn("testRole");
        given(session.getAttribute(ID_PARLOR)).willReturn("testIdParlor");
        given(session.getAttribute(ID_ACTOR)).willReturn("testIdActor");
        Map<String, String> testParameters = Map.of(NEWS_TYPE, "testType", NEWS_TITLE, "testTitle", TEXT,
                "testText", ACTOR_ROLE, "testRole", ID_PARLOR, "testIdParlor", ID_ACTOR, "testIdActor");
        given(addNewsService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.REDIRECT);
        // when
        Router actualRouter = addNewsCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);

    }
}