package by.prusenko.nikolay.command.impl.news;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.news.EditAllNewsService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.BUTTON;
import static by.prusenko.nikolay.constant.Attribute.ID_NEWS;
import static by.prusenko.nikolay.constant.Attribute.NEWS_TYPE;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;


@Test
public class EditAllNewsCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private EditAllNewsService editAllNewsService;

    private BaseCommand editAllNewsCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        editAllNewsCommand = new EditAllNewsCommand(editAllNewsService);
    }

    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(ID_NEWS)).willReturn("testIdNews");
        given(request.getParameter(NEWS_TYPE)).willReturn("testNewsType");
        given(request.getParameter(BUTTON)).willReturn("testButton");
        Map<String,String> testParameters = Map.of(ID_NEWS,"testIdNews",NEWS_TYPE,"testNewsType",BUTTON,"testButton");
        given(editAllNewsService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.REDIRECT);
        // when
        Router actualRouter = editAllNewsCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter,expectedRouter);
    }

}