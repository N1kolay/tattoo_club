package by.prusenko.nikolay.command.impl.news;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.news.HandleInactiveNewsService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class HandleInactiveNewsCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HandleInactiveNewsService handleInactiveNewsService;

    private BaseCommand handleInactiveNewsCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        handleInactiveNewsCommand = new HandleInactiveNewsCommand(handleInactiveNewsService);
    }

    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(NEWS_TYPE)).willReturn("testNewsType");
        given(request.getParameter(BUTTON)).willReturn("testButton");
        given(request.getParameter(NEWS_TITLE)).willReturn("testNewsTitle");
        given(request.getParameter(TEXT)).willReturn("testText");
        given(request.getParameter(ID_NEWS)).willReturn("testIdNews");
        Map<String, String> testParameters = Map.of(NEWS_TYPE, "testNewsType", BUTTON, "testButton", NEWS_TITLE,
                "testNewsTitle", TEXT, "testText", ID_NEWS, "testIdNews");
        given(handleInactiveNewsService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.REDIRECT);
        // when
        Router actualRouter = handleInactiveNewsCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);
    }

}