package by.prusenko.nikolay.command.impl.news;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.news.InactiveNewsService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.STATUS;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;


@Test
public class InactiveNewsCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private InactiveNewsService inactiveNewsService;

    private BaseCommand inactiveNewsCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        inactiveNewsCommand = new InactiveNewsCommand(inactiveNewsService);
    }

    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(STATUS)).willReturn("testStatus");
        Map<String,String> testParameters = Map.of(STATUS,"testStatus");
        given(inactiveNewsService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.FORWARD);
        // when
        Router actualRouter = inactiveNewsCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter,expectedRouter);
    }
}