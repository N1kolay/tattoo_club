package by.prusenko.nikolay.command.impl.news;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.news.NewsService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.NEWS_TYPE;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class NewsCommandTest {
    @Mock
    private HttpServletRequest request;
    @Mock
    private NewsService newsService;

    private BaseCommand newsCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        newsCommand = new NewsCommand(newsService);
    }

    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(NEWS_TYPE)).willReturn("testNewsType");
        Map<String, String> testParameters = Map.of(NEWS_TYPE, "testNewsType");
        given(newsService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.FORWARD);
        // when
        Router actualRouter = newsCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);
    }

}