package by.prusenko.nikolay.command.impl.parlor;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.parlor.AddParlorService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

public class AddParlorCommandTest extends Mockito {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private AddParlorService addParlorService;

    private BaseCommand addParlorCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        addParlorCommand = new AddParlorCommand(addParlorService);
    }

    @Test
    public void shouldSuccessfullyRetrieveRouter() throws DaoException, ServiceException {
        // given
        given(request.getParameter(PARLOR_NAME)).willReturn("testParlorName");
        given(request.getParameter(PARLOR_COUNTRY)).willReturn("testParlorCountry");
        given(request.getParameter(PARLOR_CITY)).willReturn("testParlorCity");
        given(request.getParameter(PARLOR_ADDRESS)).willReturn("testParlorAddress");
        given(request.getSession()).willReturn(session);
        given(session.getAttribute(ACTOR_NAME)).willReturn("testActorName");
        Map<String, String> testParameters = Map.of(PARLOR_NAME, "testParlorName", PARLOR_COUNTRY, "testParlorCountry",
                PARLOR_CITY, "testParlorCity", PARLOR_ADDRESS, "testParlorAddress", ACTOR_NAME, "testActorName");
        given(addParlorService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.REDIRECT);
        // when
        Router actualRouter = addParlorCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);

    }
}