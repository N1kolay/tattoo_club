package by.prusenko.nikolay.command.impl.parlor;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.constant.Attribute;
import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.dao.impl.ParlorDaoImpl;
import by.prusenko.nikolay.entity.Parlor;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.parlor.AllParlorsService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.STATUS;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.MockitoAnnotations.initMocks;


public class AllParlorCommandTest extends Mockito {
    @Mock
    private HttpServletRequest request;
    @Mock
    private ParlorDao parlorDao;
    @Mock
    private  AllParlorsService allParlorsService;

    private BaseCommand allParlorCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        allParlorCommand = new AllParlorCommand(allParlorsService);
    }

    @Test
    public void shouldSuccessfullyRetrieveRouter() throws DaoException, ServiceException {
        // given
        given(request.getParameter(STATUS)).willReturn("testStatus");
        Map<String,String> testParameters = Map.of(STATUS,"testStatus");
        List<Parlor> list = new ArrayList<>();
        given(allParlorsService.processAction(testParameters)).willReturn("testPath");
        willReturn(list).given(parlorDao).findActiveParlor("active");
        Router expectedRouter = new Router("testPath", Router.RouterType.FORWARD);
        // when
        Router actualRouter = allParlorCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);

    }


}