package by.prusenko.nikolay.command.impl.parlor;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.entity.Parlor;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.parlor.DeleteParlorService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.ID_ACTOR;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

public class DeleteParlorCommandTest extends Mockito {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpSession session;
    @Mock
    private DeleteParlorService deleteParlorService;

    private BaseCommand deleteParlorCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        deleteParlorCommand = new DeleteParlorCommand(deleteParlorService);
    }

    @Test
    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getSession()).willReturn(session);
        given(session.getAttribute(ID_ACTOR)).willReturn("testIdActor");
        Map<String, String> testParameters = Map.of(ID_ACTOR, "testIdActor");
        given(deleteParlorService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.FORWARD);
        // when
        Router actualRouter = deleteParlorCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);
    }

}