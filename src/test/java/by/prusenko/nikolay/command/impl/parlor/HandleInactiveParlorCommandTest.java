package by.prusenko.nikolay.command.impl.parlor;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.dao.impl.ParlorDaoImpl;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.parlor.DeleteParlorService;
import by.prusenko.nikolay.service.impl.parlor.HandleInactiveParlorService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;


public class HandleInactiveParlorCommandTest extends Mockito {
    @Mock
    private HttpServletRequest request;
    @Mock
    private HandleInactiveParlorService handleInactiveParlorService;

    private BaseCommand handleInactiveParlorCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        handleInactiveParlorCommand = new HandleInactiveParlorCommand(handleInactiveParlorService);
    }

    @Test
    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(ID_PARLOR)).willReturn("3");
        given(request.getParameter(BUTTON)).willReturn("add");
        Map<String, String> testParameters =  Map.of(ID_PARLOR,"3",BUTTON,"add");
        given(handleInactiveParlorService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.REDIRECT);
        // when
        Router actualRouter = handleInactiveParlorCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);
    }
}