package by.prusenko.nikolay.command.impl.parlor;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.constant.Attribute;
import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.dao.impl.ParlorDaoImpl;
import by.prusenko.nikolay.entity.Parlor;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.parlor.InactiveParlorService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.BUTTON;
import static by.prusenko.nikolay.constant.Attribute.ID_PARLOR;
import static by.prusenko.nikolay.constant.Attribute.STATUS;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;


public class InactiveParlorCommandTest extends Mockito {
    @Mock
    private HttpServletRequest request;
    @Mock
    private InactiveParlorService inactiveParlorService;

    private BaseCommand inactiveParlorCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        inactiveParlorCommand = new InactiveParlorCommand(inactiveParlorService);
    }

    @Test
    public void shouldUnsuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(STATUS)).willReturn("testStatus");
        Map <String,String> testParameters = Map.of(STATUS,"testStatus");
        given(inactiveParlorService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.REDIRECT);
        // when
        Router actualRouter = inactiveParlorCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter, expectedRouter);
    }

}