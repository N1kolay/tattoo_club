package by.prusenko.nikolay.command.impl.parlor;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.dao.impl.ParlorDaoImpl;
import by.prusenko.nikolay.entity.Parlor;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.service.impl.parlor.ParlorByCityService;
import by.prusenko.nikolay.util.Router;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import javax.servlet.http.HttpServletRequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.PARLOR_CITY;
import static by.prusenko.nikolay.constant.Attribute.PARLOR_COUNTRY;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;


public class ParlorByCityCommandTest extends Mockito{
    @Mock
    private HttpServletRequest request;
    @Mock
    private ParlorByCityService parlorByCityService;

    private BaseCommand parlorByCityCommand;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        parlorByCityCommand = new ParlorByCityCommand(parlorByCityService);
    }

    @Test
    public void shouldSuccessfullyRetrieveRouter() throws ServiceException {
        // given
        given(request.getParameter(PARLOR_COUNTRY)).willReturn("testParlorCountry");
        given(request.getParameter(PARLOR_CITY)).willReturn("testParlorCity");
        Map<String,String> testParameters = Map.of(PARLOR_COUNTRY, "testParlorCountry", PARLOR_CITY, "testParlorCity");
        given(parlorByCityService.processAction(testParameters)).willReturn("testPath");
        Router expectedRouter = new Router("testPath", Router.RouterType.FORWARD);
        // when
        Router actualRouter = parlorByCityCommand.execute(request);
        // then
        Assert.assertEquals(actualRouter,expectedRouter);
    }

}