package by.prusenko.nikolay.pool;

import by.prusenko.nikolay.dao.pool.ConnectionPool;
import org.junit.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ConnectionPoolTest {

    private Connection allConnections[];
    private ConnectionPool connectionPool;
    private String simpleQuery;

    @BeforeMethod
    public void initConnectionPool() {
        Properties properties = new Properties();
        connectionPool = ConnectionPool.getInstance();
        properties.setProperty("numberOfConnections", String.valueOf(4));
        simpleQuery = "SELECT * FROM actor";
        int numberOfConnections = Integer.parseInt(properties.getProperty("numberOfConnections"));
        allConnections = new Connection[numberOfConnections];
    }

    @AfterMethod
    public void close() {
        for (int i = 0; i < allConnections.length; i++) {
            connectionPool.returnConnection(allConnections[i]);
            allConnections[i] = null;
        }
        connectionPool.closePool();
    }

    @Test
    public void shouldSuccessfullyTestConnectionPool() throws SQLException {
        for (int i = 0; i < allConnections.length; i++) {
            allConnections[i] = connectionPool.receiveConnection();
            Statement stmt = allConnections[i].createStatement();
            stmt.executeQuery(simpleQuery);
        }
    }
}

