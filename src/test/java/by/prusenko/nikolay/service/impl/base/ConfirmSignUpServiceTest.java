package by.prusenko.nikolay.service.impl.base;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.UserDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.util.SecurePassword;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.ACTOR_NAME;
import static by.prusenko.nikolay.constant.Attribute.TOKEN;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class ConfirmSignUpServiceTest {
    @Mock
    private UserDao userDao;
    @Mock
    private ConfirmSignUpService confirmSignUpService;


    @BeforeMethod
    public void setUp(){
        initMocks(this);
        confirmSignUpService = new ConfirmSignUpService(userDao);

    }

    @Test
    public void shouldSuccessfullyRetrievePath() throws DaoException, ServiceException {
        //given
        String token = SecurePassword.hash("testName");
        Map<String, String> testParameters = Map.of(ACTOR_NAME, "testName", TOKEN, token);
        doNothing().when(userDao).updateStatus("testName");
        //when
        String actualPath = confirmSignUpService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.MAIN);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void shouldUnsuccessfullyRetrievePath() throws DaoException, ServiceException {
        //given
        Map<String, String> testParameters = Map.of(ACTOR_NAME, "testName", TOKEN, "testToken");
        verify(userDao, times(0)).updateStatus("testName");
        doNothing().when(userDao).updateStatus(isA(String.class));
        //when
        String actualPath = confirmSignUpService.processAction(testParameters);
        // then
        Assert.assertNotEquals(actualPath, Page.MAIN);

    }

}