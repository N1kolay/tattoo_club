package by.prusenko.nikolay.service.impl.base;

import by.prusenko.nikolay.constant.Page;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.LANG;
import static by.prusenko.nikolay.constant.Attribute.PATH;
import static org.mockito.MockitoAnnotations.initMocks;

public class LanguageServiceTest {

    private LanguageService languageService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        languageService = new LanguageService();
    }

    @Test
    public void shouldSuccessfullyRetrieveRequestParameters() {
        //given
        Map<String, String> testParameters = Map.of(LANG, "ru_RU", PATH, "/jsp/main.jsp");
        // when
        String actualPath = languageService.processAction(testParameters);
        //then
        Assert.assertEquals(actualPath, Page.MAIN);
    }

    @Test
    public void shouldUnsuccessfullyRetrieveRequestParameters() {
        //given
        String incorrectPath = "main.jsp";
        Map<String, String> testParameters = Map.of(LANG, "ru_RU", PATH, incorrectPath);
        //when
        String actualPath = languageService.processAction(testParameters);
        //then
        Assert.assertNotEquals(actualPath, Page.MAIN);
    }

}