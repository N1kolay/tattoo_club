package by.prusenko.nikolay.service.impl.base;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.UserDao;
import by.prusenko.nikolay.entity.ActorRole;
import by.prusenko.nikolay.entity.User;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;
import java.util.Optional;

import static by.prusenko.nikolay.constant.Attribute.ACTOR_EMAIL;
import static by.prusenko.nikolay.constant.Attribute.ACTOR_PASSWORD;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class LoginServiceTest {
    @Mock
    private UserDao userDao;
    @Mock
    private LoginService loginService;


    @BeforeMethod
    public void setUp() {
        initMocks(this);
        loginService = new LoginService(userDao);
    }

    @Test
    public void shouldSuccessfullyRetrievePath() throws DaoException, ServiceException {
        //given
        Map<String, String> testParameters = Map.of(ACTOR_EMAIL, "testEmail", ACTOR_PASSWORD, "testPassword");
        User user = new User("testName", ActorRole.USER, 1, 1);
        Optional<User> userOptional = Optional.of(user);
        given(userDao.findUser("testEmail", "testPassword")).willReturn(userOptional);
        // when
        String actualPath = loginService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.MAIN);
    }

    @Test
    public void shouldUnsuccessfullyRetrievePath() throws DaoException, ServiceException {
        // given
        Map<String, String> testParameters = Map.of(ACTOR_EMAIL, "testEmail", ACTOR_PASSWORD, "testPassword");
        Optional<User> optionalUser = Optional.empty();
        given(userDao.findUser("testEmail", "testPassword")).willReturn(optionalUser);
        //when
        String actualPath = loginService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.LOGIN + "?message=error");
    }
}