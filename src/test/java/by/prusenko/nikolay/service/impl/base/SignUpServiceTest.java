package by.prusenko.nikolay.service.impl.base;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.UserDao;
import by.prusenko.nikolay.dao.impl.UserDaoImpl;
import by.prusenko.nikolay.entity.User;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import by.prusenko.nikolay.util.SecurePassword;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyObject;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class SignUpServiceTest {
    @Mock
    private UserDao userDao;
    @Mock
    private SignUpService signUpService;


    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        signUpService = new SignUpService(userDao);
    }

    @Test
    public void shouldSuccessfullyRetrievePath() throws DaoException, ServiceException {
        // given
        Map<String, String> testParameters = Map.of(ACTOR_NAME, "testName", ACTOR_EMAIL, "testEmail@gmail.com",
                ACTOR_PASSWORD, "testPassword1", ACTOR_PASSWORD_CONFIRMATION, "testPassword1");
        given(userDao.uniqueNameEmail("testName", "testEmail@gmail.com")).willReturn(true);
        given(userDao.addUser(any(User.class))).willReturn(true);
        // when
        String actualPath = signUpService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.LOGIN);
    }

    @Test
    public void shouldUnsuccessfullyRetrievePath() throws DaoException, ServiceException {
        //given
        Map<String, String> testParameters = Map.of(ACTOR_NAME, "testName", ACTOR_EMAIL, "testEmail",
                ACTOR_PASSWORD, "testPassword", ACTOR_PASSWORD_CONFIRMATION, "testPassConfirm");
        given(userDao.uniqueNameEmail("testName", "testEmail")).willReturn(true);
        given(userDao.addUser(anyObject())).willReturn(true);
        // when
        String actualPath = signUpService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.SIGN_UP);
    }

}