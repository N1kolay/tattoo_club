package by.prusenko.nikolay.service.impl.feedback;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.FeedbackDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class AddAnswerServiceTest {
    @Mock
    private FeedbackDao feedbackDao;

    private AddAnswerService addAnswerService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        addAnswerService = new AddAnswerService(feedbackDao);
    }

    public void shouldSuccessfullyRetrievePath() throws DaoException, ServiceException {
        // given
        int idParlor = 3;
        Map<String, String> testParameters = Map.of(ANSWER, "testAnswer", ID_FEEDBACK, "2",
                ID_ACTOR, "3", ID_PARLOR, "3");
        willDoNothing().given(feedbackDao).addAnswer("testAnswer", 2, 3);
        // when
        String actualPath = addAnswerService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.FEEDBACK_COMMAND + idParlor);
    }
}