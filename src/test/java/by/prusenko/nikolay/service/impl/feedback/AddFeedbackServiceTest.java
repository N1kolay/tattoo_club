package by.prusenko.nikolay.service.impl.feedback;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.FeedbackDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class AddFeedbackServiceTest {
    @Mock
    private FeedbackDao feedbackDao;

    private AddFeedbackService addFeedbackService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        addFeedbackService = new AddFeedbackService(feedbackDao);
    }

    public void shouldSuccessfullyRetrievePath() throws DaoException, ServiceException {
        // given
        int idParlor = 5;
        Map<String, String> testParameters = Map.of(ID_ACTOR, "4", RATING, "testRating", COMMENT,
                "testComment", ID_PARLOR, "5");
        willDoNothing().given(feedbackDao).addFeedback("testComment", "testRating", 5,
                "5");
        // when
        String actualPath = addFeedbackService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.FEEDBACK_COMMAND + idParlor);
    }
}