package by.prusenko.nikolay.service.impl.feedback;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.FeedbackDao;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class DeleteFeedbackServiceTest {
    @Mock
    private FeedbackDao feedbackDao;

    private DeleteFeedbackService deleteFeedbackService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        deleteFeedbackService = new DeleteFeedbackService(feedbackDao);
    }

    public void shouldSuccessfullyRetrievePath() throws ServiceException {
        // given
        int idParlor = 5;
        Map<String, String> testParameters = Map.of(ID_FEEDBACK, "3", ANSWER, "testAnswer",
                ID_PARLOR, "5");
        // when
        String actualPath = deleteFeedbackService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.FEEDBACK_COMMAND + idParlor);
    }
}