package by.prusenko.nikolay.service.impl.feedback;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.FeedbackDao;
import by.prusenko.nikolay.entity.Feedback;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.ID_PARLOR;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class FeedbackServiceTest {
    @Mock
    private FeedbackDao feedbackDao;

    private FeedbackService feedbackService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        feedbackService = new FeedbackService(feedbackDao);
    }

    public void shouldSuccessfullyRetrievePath() throws DaoException, ServiceException {
        // given
        Map<String, String> testParameters = Map.of(ID_PARLOR, "5");
        given(feedbackDao.findFeedbackByParlor(5)).willReturn(new ArrayList<>(););
        // when
        String actualPath = feedbackService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.FEEDBACK);
    }
}