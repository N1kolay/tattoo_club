package by.prusenko.nikolay.service.impl.news;

import by.prusenko.nikolay.command.impl.news.AddNewsCommand;
import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.NewsDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class AddNewsServiceTest {
    @Mock
    private NewsDao newsDao;

    private AddNewsService addNewsService;
    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        addNewsService = new AddNewsService(newsDao);
    }

    public void shouldSuccessfullyRetrievePath() throws DaoException, ServiceException {
        // given
        Map<String, String> testParameters = Map.of(NEWS_TYPE, "testType", NEWS_TITLE, "testTitle", TEXT,
                "testText", ACTOR_ROLE, "testRole", ID_PARLOR, "testIdParlor", ID_ACTOR, "testIdActor");
        willDoNothing().given(newsDao).addNews("sql", "testTitle", "testType", "testText", 1);
        // when
        String actualPath = addNewsService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.ADD_NEWS);
    }

}