package by.prusenko.nikolay.service.impl.news;

import by.prusenko.nikolay.command.BaseCommand;
import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.NewsDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.BUTTON;
import static by.prusenko.nikolay.constant.Attribute.ID_NEWS;
import static by.prusenko.nikolay.constant.Attribute.NEWS_TYPE;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.*;

@Test
public class EditAllNewsServiceTest {
    @Mock
    private NewsDao newsDao;

    private EditAllNewsService editAllNewsService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        editAllNewsService = new EditAllNewsService(newsDao);
    }

    public void shouldSuccessfullyRetrievePath() throws ServiceException, DaoException {
        // given
        String newsType = "news";
        Map<String,String> testParameters = Map.of(ID_NEWS,"1",NEWS_TYPE,newsType,BUTTON,"edit");
        verify(newsDao,times(0)).deleteNews(Integer.parseInt("1"));
        // when
        String actualPath = (editAllNewsService.processAction(testParameters));
        // then
        Assert.assertEquals(actualPath, Page.COMMAND+newsType);
    }

}