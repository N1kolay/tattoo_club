package by.prusenko.nikolay.service.impl.news;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.NewsDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class HandleInactiveNewsServiceTest {
    @Mock
    private NewsDao newsDao;

    private HandleInactiveNewsService handleInactiveNewsService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        handleInactiveNewsService = new HandleInactiveNewsService(newsDao);
    }

    public void shouldSuccessfullyRetrievePathWithButtonAdd() throws ServiceException, DaoException {
        // given
        Map<String, String> testParameters = Map.of(NEWS_TYPE, "testNewsType", BUTTON, "add", NEWS_TITLE,
                "testNewsTitle", TEXT, "testText", ID_NEWS, "3");
        willDoNothing().given(newsDao).addNews("sql", "testNewsTitle", "testNewsType", "testText", 3);
        verify(newsDao,times(0)).deleteNews(3);
        // when
        String actualPath = handleInactiveNewsService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.INACTIVE_NEWS_COMMAND);
    }

    public void shouldSuccessfullyRetrievePathWithButtonDelete() throws ServiceException, DaoException {
        // given
        Map<String, String> testParameters = Map.of(NEWS_TYPE, "testNewsType", BUTTON, "delete", NEWS_TITLE,
                "testNewsTitle", TEXT, "testText", ID_NEWS, "3");
        willDoNothing().given(newsDao).addNews("sql", "testNewsTitle", "testNewsType", "testText", 3);
        // when
        String actualPath = handleInactiveNewsService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.INACTIVE_NEWS_COMMAND);
    }

}