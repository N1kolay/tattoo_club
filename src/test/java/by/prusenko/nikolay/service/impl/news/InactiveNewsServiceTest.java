package by.prusenko.nikolay.service.impl.news;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.NewsDao;
import by.prusenko.nikolay.entity.News;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.STATUS;
import static org.mockito.BDDMockito.given;
import static org.mockito.MockitoAnnotations.initMocks;


@Test
public class InactiveNewsServiceTest {
    @Mock
    private NewsDao newsDao;

    private InactiveNewsService inactiveNewsService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        inactiveNewsService = new InactiveNewsService(newsDao);
    }

    public void shouldSuccessfullyRetrievePath() throws DaoException, ServiceException {
        // given
        Map<String,String> testParameters = Map.of(STATUS,"testStatus");
        given(newsDao.findInactiveNews("testStatus")).willReturn(new ArrayList<>());
        // when
        String actualPath = inactiveNewsService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.INACTIVE_NEWS);
    }
}