package by.prusenko.nikolay.service.impl.news;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.NewsDao;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.NEWS_TYPE;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class NewsServiceTest {
    @Mock
    private NewsDao newsDao;

    private NewsService newsService;


    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        newsService = new NewsService(newsDao);
    }

    public void shouldSuccessfullyRetrievePathWithCorrectType() throws ServiceException {
        // given
        Map<String, String> testParameters = Map.of(NEWS_TYPE, "news");
        // when
        String actualPath = newsService.processAction(testParameters);
        //then
        Assert.assertEquals(actualPath, Page.NEWS);
    }

    public void shouldSuccessfullyRetrievePathWithIncorrectType() throws ServiceException {
        // given
        Map<String, String> testParameters = Map.of(NEWS_TYPE, "bla");
        // when
        String actualPath = newsService.processAction(testParameters);
        //then
        Assert.assertEquals(actualPath, Page.MAIN);
    }

}