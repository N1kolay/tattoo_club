package by.prusenko.nikolay.service.impl.parlor;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.*;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Matchers.isA;
import static org.mockito.MockitoAnnotations.initMocks;

public class AddParlorServiceTest {
    @Mock
    private ParlorDao parlorDao;

    private AddParlorService addParlorService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        addParlorService = new AddParlorService(parlorDao);
    }

    @Test
    public void shouldSuccessfullyRetrievePath() throws DaoException, ServiceException {
        // given
        Map<String, String> testParameters = Map.of(PARLOR_NAME, "testParlorName", PARLOR_COUNTRY, "testParlorCountry", PARLOR_CITY, "testParlorCity", PARLOR_ADDRESS, "testParlorAddress",
                ACTOR_NAME, "testActorName");
        willDoNothing().given(parlorDao).chainParlorUser(isA(String.class), isA(String.class), isA(String.class), isA(String.class), isA(String.class));
        given(parlorDao.findParlorByAddress(isA(String.class), isA(String.class))).willReturn(1);
        // when
        String actualPath = addParlorService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.PROFILE);
    }

}