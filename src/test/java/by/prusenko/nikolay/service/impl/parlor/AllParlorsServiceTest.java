package by.prusenko.nikolay.service.impl.parlor;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.entity.Parlor;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.STATUS;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class AllParlorsServiceTest {
    @Mock
    private ParlorDao parlorDao;

    private AllParlorsService allParlorsService;


    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        allParlorsService = new AllParlorsService(parlorDao);
    }

    @Test
    public void shouldSuccessfullyRetrievePath() throws DaoException, ServiceException {
        // given
        Map<String, String> testParameters = Map.of(STATUS, "testStatus");
        willReturn(new ArrayList<>()).given(parlorDao).findActiveParlor("active");
        //when
        String actualPath = allParlorsService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.PARLORS);

    }

}