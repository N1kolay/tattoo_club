package by.prusenko.nikolay.service.impl.parlor;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.ID_ACTOR;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Matchers.isA;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class DeleteParlorServiceTest {
    @Mock
    private ParlorDao parlorDao;
    @Mock
    private DeleteParlorService deleteParlorService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        deleteParlorService = new DeleteParlorService(parlorDao);
    }

    @Test
    public void shouldSuccessfullyRetrievePath() throws DaoException, ServiceException {
        // given
        Map<String, String> testParameters = Map.of(ID_ACTOR, "1");
        willDoNothing().given(parlorDao).deleteParlorFromUser(isA(Integer.class));
        // when
        String actualPath = deleteParlorService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.PROFILE);
    }
}