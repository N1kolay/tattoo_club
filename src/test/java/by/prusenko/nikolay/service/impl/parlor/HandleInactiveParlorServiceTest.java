package by.prusenko.nikolay.service.impl.parlor;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.BUTTON;
import static by.prusenko.nikolay.constant.Attribute.ID_PARLOR;
import static org.mockito.BDDMockito.willDoNothing;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

public class HandleInactiveParlorServiceTest {
    @Mock
    private ParlorDao parlorDao;

    private HandleInactiveParlorService handleInactiveParlorService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        handleInactiveParlorService = new HandleInactiveParlorService(parlorDao);
    }

    @Test
    public void shouldSuccessfullyRetrievePathWithButtonAdd() throws DaoException, ServiceException {
        // given
        Map<String, String> testParameters = Map.of(ID_PARLOR, "3", BUTTON, "add");
        willDoNothing().given(parlorDao).activateParlor(isA(Integer.class));
        verify(parlorDao, times(0)).deleteIncorrectParlor(isA(Integer.class));
        // when
        String actualPath = handleInactiveParlorService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.INACTIVE_PARLOR_COMMAND);
    }

    @Test
    public void shouldSuccessfullyRetrievePathWithButtonDelete() throws DaoException, ServiceException {
        // given
        Map<String, String> testParameters = Map.of(ID_PARLOR, "3", BUTTON, "delete");
        willDoNothing().given(parlorDao).activateParlor(isA(Integer.class));
        verify(parlorDao, times(0)).activateParlor(isA(Integer.class));
        // when
        String actualPath = handleInactiveParlorService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.INACTIVE_PARLOR_COMMAND);
    }


}