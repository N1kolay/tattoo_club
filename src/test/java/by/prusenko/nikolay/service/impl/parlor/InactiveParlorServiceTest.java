package by.prusenko.nikolay.service.impl.parlor;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.entity.Parlor;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.STATUS;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class InactiveParlorServiceTest {
    @Mock
    private ParlorDao parlorDao;

    private InactiveParlorService inactiveParlorService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        inactiveParlorService = new InactiveParlorService(parlorDao);
    }

    @Test
    public void shouldSuccessfullyRetrievePath() throws DaoException, ServiceException {
        // given
        Map<String, String> testParameters = Map.of(STATUS, "testStatus");
        Mockito.doReturn(new ArrayList<>()).when(parlorDao).findInactiveParlor("active");
        // when
        String actualPath = inactiveParlorService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.INACTIVE_PARLOR);
    }
}