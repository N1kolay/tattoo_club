package by.prusenko.nikolay.service.impl.parlor;

import by.prusenko.nikolay.constant.Page;
import by.prusenko.nikolay.dao.ParlorDao;
import by.prusenko.nikolay.entity.Parlor;
import by.prusenko.nikolay.exception.DaoException;
import by.prusenko.nikolay.exception.ServiceException;
import org.mockito.Mock;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static by.prusenko.nikolay.constant.Attribute.PARLOR_CITY;
import static by.prusenko.nikolay.constant.Attribute.PARLOR_COUNTRY;
import static org.mockito.BDDMockito.willReturn;
import static org.mockito.MockitoAnnotations.initMocks;

@Test
public class ParlorByCityServiceTest {
    @Mock
    private ParlorDao parlorDao;

    private ParlorByCityService parlorByCityService;

    @BeforeMethod
    public void setUp() throws Exception {
        initMocks(this);
        parlorByCityService = new ParlorByCityService(parlorDao);
    }

    @Test
    public void shouldUnsuccessfullyRetrievePath() throws DaoException, ServiceException {
        // given
        Map<String, String> testParameters = Map.of(PARLOR_COUNTRY, "testParlorCountry", PARLOR_CITY, "testParlorCity");
        willReturn(new ArrayList<>()).given(parlorDao).findParlorByCity("testParlorCountry", "testParlorCity");
        // when
        String actualPath = parlorByCityService.processAction(testParameters);
        // then
        Assert.assertEquals(actualPath, Page.PARLORS);
    }
}