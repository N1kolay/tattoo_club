package by.prusenko.nikolay.util;

import org.junit.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


@Test
public class InputValidatorTest {

    @DataProvider(name="nameProvider")
    private static Object[][] nameProvider(){
        return new Object[][]{
                {"",false},
                {" ",false},
                {"qw",false},
                {"Dw",true},
                {"Nik",true},
                {"1234",false},
                {"Тест",true},
                {"TestName",false},
                {"Qwasqwasqwerdfghvbcxsdweerrtfgvb",false},
        };
    }
    @Test(dataProvider = "nameProvider")
    public void testValidateName(String name, boolean expected) {
        Assert.assertEquals(InputValidator.validateName(name), expected);
    }
    @DataProvider(name="emailProvider")
    private static Object[][] emailProvider(){
        return new Object[][]{
                {"",false},
                {" ",false},
                {"qw",false},
                {"test@gmail.com",true},
                {"test.mail@gmail.com",true},
                {"test_mail@gmail.com",true},
                {"test_2000@gmail.com",true},
                {"2000-test_2000@gmail.com",true},
                {"test@gmail.",false},
                {"test@gmail",false},
        };
    }
    @Test(dataProvider = "emailProvider")
    public void testValidateEmail(String email, boolean expected) {
        Assert.assertEquals(InputValidator.validateEmail(email), expected);
    }
}