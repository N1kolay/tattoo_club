package by.prusenko.nikolay.util;

import org.junit.Assert;
import org.mindrot.jbcrypt.BCrypt;
import org.testng.annotations.Test;



public class SecurePasswordTest {
    @Test
    public void shouldSuccessfullySecurePassword(){
        String pass = "password";
        String hash = SecurePassword.hash(pass);
        Assert.assertTrue(BCrypt.checkpw(pass,hash));
    }

    @Test
    public void shouldUnsuccessfullySecurePassword(){
        String pass = "password";
        String incorrectPass = "Password";
        String hash = SecurePassword.hash(pass);
        Assert.assertFalse(BCrypt.checkpw(incorrectPass,hash));
    }
}