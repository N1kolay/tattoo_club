<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>
<html>
<head>
    <title>Add News</title>
    <link rel="stylesheet" type="text/css" href="/css/addNews.css">
    <style>
        main{
            position: relative;
            top:80px
        }
    </style>
</head>
<body>
<header>
    <c:import url="../common/header.jsp"/>
</header>
<c:set var="path" scope="session" value="${pageContext.request.requestURI}"  />

<main align="center">
<c:if test="${sessionScope.addNewsMessage ne null}">
    <%--<c:out value="${sessionScope.addNewsMessage}"/>--%>
    <fmt:message key="message.News"/>
    <c:remove var="addNewsMessage" scope="session" />
</c:if>
    <c:choose>
        <c:when test="${sessionScope.role=='user'||sessionScope.role=='parlor'||sessionScope.role=='moderator'}">
            <form action="/TattooClub" method="post" >
                    <input type="hidden" name="command" value="addNews">
                <h1 id="header"><fmt:message key="page.news.addNews"/></h1>
                <div id="emptyDiv"></div>
                <br>
                <select required name="type" class="select">
                    <option value="" hidden><fmt:message key="page.news.chooseType"/></option>
                    <option value="news"><fmt:message key="page.news.news"/></option>
                    <option value="article"><fmt:message key="page.news.article"/></option>
                    <option value="discount"><fmt:message key="page.news.discount"/></option>
                </select>
                <input id="title" name="name" placeholder=<fmt:message key="page.news.titleForContent"/> class="box" required>
                <br>
                <textarea id="text" name="text" placeholder=<fmt:message key="page.news.textHere"/> class="boxes" required></textarea>
                <br>
                <button type="submit" id="btn" ><fmt:message key="page.button.submit"/></button>
            </form>
        </c:when>
    </c:choose>
</main>
<c:import url="/jsp/common/footer.jsp"/>
</body>
</html>
