<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="ctg" uri="customtags" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>

<html>
<head>
    <title>Profile</title>
    <style>
        main {
            position: relative;
            top: 80px;
        }
    </style>
</head>
<body>
<header>
    <c:import url="../common/header.jsp"/>
</header>
<c:set var="path" scope="session" value="${pageContext.request.requestURI}"  />
            <main align="center" width="1000">

            ${deleteParlor}
            ${parlorError}
            ${parlorAdded}


            <c:choose>
                <c:when test="${sessionScope.role eq 'user' and sessionScope.idParlor eq 0}">
                    <fmt:message key="page.parlor.add"/> <a href="/jsp/user/addParlor.jsp"><fmt:message
                        key="page.news.here"/></a>
                </c:when>
                <c:when test="${sessionScope.role eq 'user' and sessionScope.idParlor ne null}">
                    <fmt:message key="page.parlor.added"/> </a>
                </c:when>
                <c:when test="${sessionScope.role eq 'parlor' }">
                    <fmt:message key="page.parlor.delete"/> <a href="../../TattooClub?command=deleteParlor"><fmt:message
                        key="page.news.here"/></a>
                </c:when>
            </c:choose>

            </main>
            <c:import url="/jsp/common/footer.jsp"/>
            </body>
            </html>
