<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>
<html>
<head>
    <title><fmt:message key="page.menu.inactiveNews"/></title>
</head>
<c:set var="path" value="${pageContext.request.requestURI}" scope="session"/>
<body>
<header>
    <c:import url="/jsp/common/header.jsp"/>
    <style>main{position: relative;
        top:80px}</style>
</header>

<main>
<h1 id="header"><fmt:message key="page.menu.inactiveNews"/></h1>
<c:if test="${empty inactiveNews}"><fmt:message key="message.inactiveNews"/></c:if>



<c:forEach var="news" items="${inactiveNews}" >
    <form method = "post" action="/Admin">
        <input type="hidden" name="command" value="handleInactiveNews" />
        <input type="hidden" name="idNews" value="${news.id}">
        <table align="center">
            <tr><td><fmt:message key="page.news.addedBy"/> ${news.author} ${news.date}</td></tr>
            <tr><td><fmt:message key="page.news.type"/> ${news.type}</td></tr>
            <tr><td><select name="type" required>
                <option value="" hidden><fmt:message key="page.news.chooseType"/></option>
                <option value="news"><fmt:message key="page.news.news"/></option>
                <option value="article"><fmt:message key="page.news.article"/></option>
                <option value="discount"><fmt:message key="page.news.discount"/></option>
            </select></td></tr>
            <tr><td><fmt:message key="page.news.title"/> <input name="name" id="name" cols="90" rows="2" value="${news.name}"></td></tr>
            <tr> <td><textarea cols="50" rows="5" name="text" id="message" data="elastic">${news.text}</textarea></td></tr>
            <tr> <td>
                <button type="submit" name="button" value="add"><fmt:message key="page.button.add"/></button>
                <button type="submit" name="button" value="delete"><fmt:message key="page.button.delete"/></button></td></tr>
            </table>
    </form>
</c:forEach>

</main>
<footer></footer>

</body>

</html>
