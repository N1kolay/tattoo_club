<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>
<html>
<head>
    <title><fmt:message key="page.menu.inactiveParlors"/></title>
    <style>
        main{
        position: relative;
        top:80px}
        tr{
            border: 1px solid #ccc;
        }
    </style>

</head>
<body>

<header>
    <c:import url="/jsp/common/header.jsp"/>
</header>
<c:set var="path" value="${pageContext.request.requestURI}" scope="session"/>

<main>
    <h1 id="header"><fmt:message key="page.menu.inactiveParlors"/></h1>
    <c:if test="${empty unfixParlor}"><fmt:message key="message.inactiveParlors"/></c:if>
<c:choose>
    <c:when test = "${role == 'admin'}" >
    <form method = "post" action="/Admin">
        <input type="hidden" name="command" value="handleInactiveParlor" />
        <table align="center">
            <tr>
                <th></th>
                <td>№</td>
                <td><fmt:message key="page.menu.username"/></td>
                <td><fmt:message key="page.parlor.name"/></td>
                <td><fmt:message key="page.country"/></td>
                <td><fmt:message key="page.parlor.city"/></td>
                <td><fmt:message key="page.parlor.address"/></td>
            </tr>
            <c:forEach var="user" items="${unfixParlor}" varStatus="id">
                <tr>
                    <input type="hidden" name="idParlor" value="${user.parlor.id}">
                    <td><c:out value="${user.parlor.id}"/></td>
                    <td><c:out value="${user.name}"/></td>
                    <td><c:out value="${user.parlor.name}"/></td>
                    <td><c:out value="${user.parlor.country}"/></td>
                    <td><c:out value="${user.parlor.city}"/></td>
                    <td><c:out value="${user.parlor.address}"/></td>
                </tr>
                <tr><td> <button type="submit" name="button" value="add"><fmt:message key="page.button.add"/></button></td>
                <td><button type="submit" name="button" value="delete"><fmt:message key="page.button.delete"/></button></td></tr>
            </c:forEach>
        </table>
    </form>
    </c:when>
</c:choose>
</main>

<footer></footer>
</body>
</html>
