<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>
<html>
<head>
    <title><fmt:message key="page.menu.articles"/></title>
    <link rel="stylesheet" href="css/news.css">
    <style>
        tr.head {
            background-color: #4169e1;
            color: #fff
        }

        tr {
            background-color: #fff
        }

        tr.title {
            text-align: center;
            font-weight: bold
        }

        td {
            padding: 5px !important;
        }
    </style>
</head>
<body>
<header>
    <c:import url="common/header.jsp"/>
</header>
<c:set var="path" scope="session" value="${pageContext.request.queryString}"/>

<main>
    <h1 id="header"><fmt:message key="page.menu.articles"/></h1>
    <p>
    <c:choose>
        <c:when test="${sessionScope.role eq null}">
            <fmt:message key="page.news.registered"/>
        </c:when>
        <c:when test="${sessionScope.role eq 'user' or
                        sessionScope.role eq 'moderator' or
                        sessionScope.role eq 'parlor'}">
            <fmt:message key="page.news.addContent"/>
            <a href="/jsp/actor/addNews.jsp"><fmt:message key="page.news.here"/></a>
        </c:when>
    </c:choose>
    </p>
    <c:choose>
    <c:when test="${sessionScope.role eq 'admin' or  'moderator'}">
    <form action="/TattooClub" method="post" id="form">
        <input type="hidden" name="command" value="editAllNews">

        </c:when>
        </c:choose>

        <c:forEach var="article" items="${list}">
            <table class="tab">
                <tr class="head">
                    <td><c:out value="${article.author}"/></td>
                    <td align="right"><c:out value="${article.date}"/></td>
                </tr>
                <tr class="title">
                    <td colspan="2"><c:out value="${article.name}"/></td>
                </tr>
                <tr>
                    <td colspan="2"><c:out value="${article.text}"/></td>
                </tr>
                <c:choose>
                    <c:when test="${sessionScope.role eq 'admin' or 'moderator'}">
                        <input type="hidden" name="type" value="${article.type}">
                        <input type="hidden" name="idNews" value="${article.id}">
                        <tr>
                            <td>
                                <button type="submit" name=button value="delete" form="form"><fmt:message
                                        key="page.button.delete"/>
                                </button>
                            </td>
                            <td align="right">
                                <button type="submit" name=button value="edit" form="form"><fmt:message
                                        key="page.button.edit"/></button>
                            </td>
                        </tr>
                    </c:when>
                </c:choose>
            </table>
            </br>
        </c:forEach>
    </form>
</main>
<c:import url="/jsp/common/footer.jsp"/>
</body>
</html>
