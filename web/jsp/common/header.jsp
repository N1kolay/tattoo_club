<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>

    <style>
        #formLogin{
            padding: 15px; min-width:300px;}
        #edit{color: #fff;
            background-color: #080808;}
        #logOut{position: absolute;
            right: 30px;}
    </style>
</head>
<header>
    <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/jsp/main.jsp"><fmt:message key="page.menu.home"/></a>
                <c:choose>
                <c:when test = "${sessionScope.role ne null and sessionScope.role ne 'admin'}" >
                <a class="navbar-brand" href="/jsp/actor/profile.jsp"> <fmt:message key="page.menu.profile"/></a>
                </c:when>
                </c:choose>
                <a class="navbar-brand" href="../../TattooClub?command=news&type=news"><fmt:message key="page.menu.news"/></a>
                <a class="navbar-brand" href="../../TattooClub?command=news&type=article"><fmt:message key="page.menu.articles"/></a>
                <a class="navbar-brand" href="../../TattooClub?command=news&type=discount"><fmt:message key="page.menu.discounts"/></a>
                <a class="navbar-brand" href="../../TattooClub?command=allParlors&status=active"><fmt:message key="page.menu.parlors"/></a>
                <c:choose>
                <c:when test = "${sessionScope.role eq 'admin'}" >
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="navbar-brand" data-toggle="dropdown" role="button">
                            <fmt:message key="page.menu.edit"/><span class="caret"></span>
                        </a>
                        <div class="dropdown-menu" id="edit">
                            <div class="container-fluid">
                                <a class="navbar-brand" href="../Admin?command=inactiveParlor&status=inactive"><fmt:message key="page.menu.parlors"/></a>
                                <a class="navbar-brand" href="../Admin?command=inactiveNews&status=inactive"><fmt:message key="page.menu.news"/></a>
                            </div>
                        </div>
                    </li>
                    </c:when>
                    </c:choose>
                    <b><a class="navbar-brand" href="/TattooClub?command=language&lang=ru_RU">RU</a></b>
                    <b><a class="navbar-brand" href="/TattooClub?command=language&lang=eng_ENG">EN</a></b>
                    </li>
                </ul>
            </div>
            <c:choose>
                <c:when test="${sessionScope.role ne null}">
                    <a class="navbar-brand" id="logOut" href="../../TattooClub?command=logout"><b style="color: yellow">${name}</b> | <fmt:message key="page.menu.logOut"/> </a>
                </c:when>
                <c:otherwise>
                    <div id="navbar" class="navbar-collapse collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="dropdown">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                    <fmt:message key="page.menu.logIn"/><span class="caret"></span>
                                </a>
                                <div class="dropdown-menu" id="formLogin">
                                    <div class="row">
                                        <div class="container-fluid">
                                            <form name="loginForm" method="post" action="\TattooClub">
                                                <input type="hidden" name="command" value="login" />
                                                <div class="form-group">
                                                    <label><fmt:message key="page.menu.email"/></label>
                                                    <input class="form-control" name="email" id="email" type="text">
                                                </div>
                                                <div class="form-group">
                                                    <label><fmt:message key="page.menu.password"/></label>
                                                    <input class="form-control" name="password" id="password" type="password"><br>
                                                </div>
                                                <button type="submit" id="btnLogin" class="btn btn-success btn-sm"><fmt:message key="page.menu.login"/></button>
                                            </form>
                                        </div>

                                    </div>
                                </div>
                            </li>

                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">
                                    <fmt:message key="page.menu.register"/><span class="caret"></span>
                                </a>
                                <div class="dropdown-menu" id="formLogin">
                                    <div class="row">
                                        <div class="container-fluid">
                                            <form action="/TattooClub" method="post" >
                                                <input type="hidden" name="command" value="signUp" />
                                                <div class="form-group">
                                                    <label><fmt:message key="page.menu.username"/></label>
                                                    <input class="form-control" name="name" id="username" type="text"
                                                           pattern="[А-ЯA-Z][a-яa-z]{2,30}" minlength=""
                                                           title="Name shouldn't contain numbers,symbols and should start with capital letter" required>
                                                </div>
                                                <div class="form-group">
                                                    <label><fmt:message key="page.menu.email"/></label>
                                                    <input class="form-control" name="email" id="email" type="email"
                                                           pattern="[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)$"
                                                           title="Incorrect email" required>
                                                </div>
                                                <div class="form-group">
                                                    <label><fmt:message key="page.menu.password"/></label>
                                                    <input class="form-control" name="password" id="password" type="password"
                                                           pattern="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}"
                                                           title="password should contain min 6 symbols,1 capital letter, 1 number" required>
                                                </div>
                                                <div class="form-group">
                                                    <label><fmt:message key="page.menu.repeatPassword"/></label>
                                                    <input class="form-control" name="confirmPassword" id="Reppassword" type="password"
                                                           pattern="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}"
                                                           title="password should contain min 6 symbols,1 capital letter, 1 number" required>
                                                </div>

                                                <button type="submit" id="btnLogin" class="btn btn-success btn-sm"><fmt:message key="page.menu.register"/></button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>

                    </div>
                </c:otherwise>
            </c:choose>
        </div>
    </nav>
</header>