<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>
<html lang="en">
<head>

    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <title>Hello, world!</title>

</head>
<body>

<nav>

    <div class="container">
        <div id="accordion" class="card">
            <div class="card-header bg-dark">
                <span class="btn btn-primary" data-toggle="collapse">English</span>
                <span class="btn btn-danger"   data-toggle="collapse">Japanese</span>
            </div>
        </div>
    </div>
</nav>
</body>
</html>
