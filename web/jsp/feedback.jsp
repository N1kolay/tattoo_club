<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>
<html>
<head>
    <title>Feedback</title>
    <script src="js/feedback.js"></script>
    <link rel="stylesheet" href="css/feedback.css"/>

    <style>main{position: relative;
        top:80px}</style>
</head>
<body>

<header>
    <c:import url="common/header.jsp"/>
</header>
<c:set var="path" scope="session" value="${pageContext.request.queryString}" />

<main>
    <c:choose>
        <c:when test="${sessionScope.role eq 'user'}">
            <form action="TattooClub" method="post">
                <table>
                    <input type="hidden" name="command" value="addFeedback">
                    <input type="hidden" name="idParlor" value="${requestScope.idParlor}">
                    <div align="center"><b><fmt:message key="page.feedback.comment"/></b></div>
                    <div class="rate" align="center">
                        <fieldset class="rating">
                            <legend><fmt:message key="page.feedback.rate"/></legend>
                            <input type="radio" id="star5" name="rating" value="5"/><label for="star5" title="Rocks!">5
                            stars</label>
                            <input type="radio" id="star4" name="rating" value="4"/><label for="star4"
                                                                                           title="Pretty good">4
                            stars</label>
                            <input type="radio" id="star3" name="rating" value="3"/><label for="star3" title="Meh">3
                            stars</label>
                            <input type="radio" id="star2" name="rating" value="2"/><label for="star2"
                                                                                           title="Kinda bad">2
                            stars</label>
                            <input type="radio" id="star1" name="rating" value="1"/><label for="star1"
                                                                                           title="Sucks big time">1
                            star</label>
                        </fieldset>
                    </div>
                    <tr>
                        <td><textarea required name="comment" cols="30" rows="10"></textarea></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value=<fmt:message key="page.button.submit"/>></td>
                    </tr>
                </table>
            </form>

            <br>
        </c:when>
    </c:choose>
    <c:forEach var="feedback" items="${requestScope.feedback}">
        <table class="tab">
            <tr>
                <td><fmt:message key="page.feedback.feedbackFrom"/><c:out value="${feedback.author}"/></td>
                <c:if test="${feedback.rating ne '0'}">
                    <td><fmt:message key="page.feedback.rating"/> <c:out value="${feedback.rating}"/></td>
                </c:if>
                <td align="right"><c:out value="${feedback.date}"/></td>
            </tr>
            <tr>
                <td colspan="3"><c:out value="${feedback.comment}"/></td>
            </tr>
        </table>
        <c:choose>
            <c:when test="${feedback.answer eq null and sessionScope.role eq 'admin' or 'moderator'}">
                <form action="TattooClub" method="post">
                    <table>
                        <tr><td> <input type="hidden" name="command" value="deleteFeedback"></td></tr>
                        <tr><td><input type="hidden" name="idFeedback" value="${feedback.id}"></td></tr>
                        <tr><td><input type="hidden" name="idParlor" value="${feedback.idParlor}"></td></tr>
                        <tr>
                            <td><button type="submit" value="Delete"><fmt:message key="page.button.delete"/></button>
                        </tr>
                    </table>
                </form>
                <br>
            </c:when>
            <c:when test="${feedback.answer ne null}">
                <table class="tab">
                    <tr>
                        <td align="right"><fmt:message key="page.feedback.answerFrom"/><c:out value="${feedback.parlor}"/></td>
                    </tr>

                    <tr>
                        <td align="right"><c:out value="${feedback.answer}"/></td>
                    </tr>
                </table>
                <c:if test="${feedback.answer ne null and sessionScope.role eq 'admin' or 'moderator'}">
                    <form action="TattooClub" method="post">
                        <table>
                            <tr><td> <input type="hidden" name="command" value="deleteFeedback"></td></tr>
                            <tr><td><input type="hidden" name="idFeedback" value="${feedback.id}"></td></tr>
                            <tr><td><input type="hidden" name="answer" value="${feedback.answer}"></td></tr>
                            <tr><td><input type="hidden" name="idParlor" value="${feedback.idParlor}"></td></tr>
                            <tr>
                                <td><input type="submit" value="Delete">
                            </tr>
                        </table>
                    </form>
                    <br>
                </c:if>
                <br>
            </c:when>
            <c:when test="${feedback.answer eq null and sessionScope.idParlor eq feedback.idParlor}">
                <form action="TattooClub" method="post">
                    <table>
                    <input type="hidden" name="command" value="addAnswer">
                    <input type="hidden" name="idParlor" value="${feedback.idParlor}">
                    <input type="hidden" name="idFeedback" value="${feedback.id}">
                    <tr>
                        <td><textarea required name="answer" cols="30" rows="10"></textarea></td>
                    </tr>
                    <tr>
                        <td><input type="submit" value="Send"></td>
                    </tr>
                    </table>
                </form>
            </c:when>
            <c:otherwise>
                <br>
            </c:otherwise>
        </c:choose>
    </c:forEach>
</main>
<c:import url="/jsp/common/footer.jsp"/>
</body>
</html>
