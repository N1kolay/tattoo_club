<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>
<html>
<head>
    <title><fmt:message key="page.menu.singIn"/></title>
    <style>
    .alert{margin-top: 50px;}
    .form{margin-top: 80px;}
    </style>
</head>
<body>
<header>
    <c:import url="common/header.jsp"/>
</header>
<c:set var="path" scope="session" value="${pageContext.request.requestURI}"/>

<c:choose>
    <c:when test="${param.message eq 'error'}">
        <div class="alert alert-danger" align="center">
            <fmt:message key="message.loginError"/>
        </div>
    </c:when>
</c:choose>
${wrongAction}
${errorLoginEmail}
${nullPage}
<c:choose>
    <c:when test="${requestScope.message ne null}">
        <div class="alert alert-success" align="center">
            ${message}
        </div>
    </c:when>
</c:choose>


<form name="loginForm" class="form" method="POST" action="\TattooClub">

    <input type="hidden" name="command" value="login"/>
    <table align="center">
        <tr>
            <td><label for="email"><fmt:message key="page.menu.email"/></label></td>
            <td><input type="text" id="email" name="email" required></td>
        </tr>
        <tr>
            <td><label for="password"><fmt:message key="page.menu.password"/></label></td>
            <td><input type="password" id="password" name="password" required></td>
        </tr>
        <tr>
            <td><input type="submit" name="OK"></td>
        </tr>
    </table>

</form>
<c:import url="/jsp/common/footer.jsp"/>
</body>
</html>
