<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>

<html>
<head><title><fmt:message key="page.menu.main"/></title>
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <link rel="stylesheet" href="/css/main.css">
    <script src="/js/main.js"></script>
    <style>
        .carousel-caption, .carousel-indicators{
            margin-bottom: 40px;
        }
    </style>
</head>
<body>
<header>
    <c:import url="/jsp/common/header.jsp"/>
</header>
<c:set var="path" scope="session" value="${pageContext.request.requestURI}"  />
<main>
    <div id="mycarousel" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            <li data-target="#mycarousel" data-slide-to="0" class="active"></li>
            <li data-target="#mycarousel" data-slide-to="1"></li>
            <li data-target="#mycarousel" data-slide-to="2"></li>
            <li data-target="#mycarousel" data-slide-to="3"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item">
                <img src="/images/1.jpg" data-color="lightblue" alt="First Image">
                <div class="carousel-caption">
                    <h3><fmt:message key="page.slider.news"/></h3>
                </div>
            </div>
            <div class="item">
                <img src="/images/21.jpg" data-color="firebrick" alt="Second Image">
                <div class="carousel-caption">
                    <h3><fmt:message key="page.slider.articles"/></h3>
                </div>
            </div>
            <div class="item">
                <img src="/images/3.jpg" data-color="violet" alt="Third Image">
                <div class="carousel-caption">
                    <h3><fmt:message key="page.slider.discounts"/></h3>
                </div>
            </div>
            <div class="item">
                <img src="/images/4.jpg" data-color="lightgreen" alt="Fourth Image">
                <div class="carousel-caption">
                    <h3><fmt:message key="page.slider.feedback"/></h3>
                </div>
            </div>
        </div>

        <a class="left carousel-control" href="#mycarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#mycarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
    <script>
        var $item = $('.carousel .item');
        var $wHeight = $(window).height();
        $item.eq(0).addClass('active');
        $item.height($wHeight);
        $item.addClass('full-screen');

        $('.carousel img').each(function() {
            var $src = $(this).attr('src');
            var $color = $(this).attr('data-color');
            $(this).parent().css({
                'background-image' : 'url(' + $src + ')',
                'background-color' : $color
            });
            $(this).remove();
        });

        $(window).on('resize', function (){
            $wHeight = $(window).height();
            $item.height($wHeight);
        });

        $('.carousel').carousel({
            interval: 6000,
            pause: "false"
        });
    </script>
</main>
<footer>
<c:import url="/jsp/common/footer.jsp"/>
</footer>
</body>
</html>
