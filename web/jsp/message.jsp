<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>
<html>
<head>
    <title>Message</title>
    <style>
        .msg{margin-top: 100px;!important;}
    </style>
</head>
<body>
<header>
    <c:import url="/jsp/common/header.jsp"/>
</header>
<c:set var="path" scope="session" value="${pageContext.request.requestURI}"  />

<c:if test="${param.message ne null}">
    <div class="msg" align="center">
    <h1>${param.message}</h1>
        <img src="/images/error.jpg" alt="error"/>
    </div>
</c:if>
<c:import url="/jsp/common/footer.jsp"/>
</body>
</html>
