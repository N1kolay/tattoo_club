<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>
<html>
<head>
    <title>News</title>
    <link rel="stylesheet" href="/css/news.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    <style>
        tr.head{background-color: #4169e1;
        color: #fff}
        tr{background-color: #fff}
        tr.title{text-align:center; font-weight: bold}
        td {padding:5px!important;}
    </style>
</head>
<body>

<header>
    <c:import url="common/header.jsp"/>
</header>
<c:set var="path" scope="session" value="${pageContext.request.queryString}" />
<main>
    <h1 id="header"><fmt:message key="page.menu.news"/></h1>
    <p>
    <c:choose>
        <c:when test="${sessionScope.role eq null}">
            <fmt:message key="page.news.registered"/>
        </c:when>
        <c:when test="${sessionScope.role eq 'user' or
                        sessionScope.role eq 'moderator' or
                        sessionScope.role eq 'parlor'}">
            <fmt:message key="page.news.addContent"/>
            <a href="/jsp/actor/addNews.jsp"><fmt:message key="page.news.here"/></a>
        </c:when>
    </c:choose>
    </p>

    <c:choose>
    <c:when test="${sessionScope.role eq 'admin' or  'moderator'}">
    <form action="/TattooClub" method="post" id="form">
        <input type="hidden" name="command" value="editAllNews">
        </c:when>
        </c:choose>
        <c:forEach var="news" items="${list}">
            <table class="tab">
                <tr class="head">
                    <td><c:out value="${news.author}"/></td>
                    <td align="right"><c:out value="${news.date}"/></td>
                </tr>
                <tr class="title">
                    <td colspan="2"><c:out value="${news.name}"/></td>
                </tr>
                <tr>
                    <td colspan="2"><c:out value="${news.text}"/></td>
                </tr>
                <c:choose>
                    <c:when test="${sessionScope.role eq 'admin' or 'moderator'}">
                        <input type="hidden" name="type" value="${news.type}">
                        <input type="hidden" name="idNews" value="${news.id}">
                        <tr>
                            <td>
                                <button type="submit" name=button value="delete" form="form"><fmt:message key="page.button.delete"/>
                                </button>
                            </td>
                            <td align="right">
                                <button type="submit" name=button
                                        value="edit" form="form"><fmt:message key="page.button.edit"/></button>
                            </td>
                        </tr>
                    </c:when>
                </c:choose>

            </table>
            <br>
        </c:forEach>
    </form>
</main>
<c:import url="/jsp/common/footer.jsp"/>
</body>
</html>
