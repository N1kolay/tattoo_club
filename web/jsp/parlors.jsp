<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>
<html>
<head>
    <title><fmt:message key="page.button.feedback"/></title>
    <script src="/js/parlor.js"></script>
    <style>main{position: relative;
        top:80px}
    .submit{margin:20px;}</style>
</head>
<body>
<header>
    <c:import url="common/header.jsp"/>
</header>
<c:set var="path" scope="session" value="${pageContext.request.queryString}" />


<main align="center" width="1000">

    <form action="/TattooClub" method="post">
        <input type="hidden" name="command" value="parlorByCity">
        <select required name="country" id="country" value="">
            <option selected disabled value=''><fmt:message key="page.country.choose"/></option>
            <option value="Belarus"><fmt:message key="page.country.by"/></option>
            <option value="Russia"><fmt:message key="page.country.ru"/></option>
            <option value="Ukraine"><fmt:message key="page.country.ukr"/></option>

        </select>
        <select required name="city" id="city" value="">
            <option class="select" selected disabled value=''><fmt:message key="page.city.choose"/></option>
            <option class="Russia" value="Moscow"><fmt:message key="page.city.msc"/></option>
            <option class="Russia" value="Saint Petersburg"><fmt:message key="page.city.spb"/></option>
            <option class="Russia" value="Volgograd"><fmt:message key="page.city.vlg"/></option>
            <option class="Ukraine" value="Kiev"><fmt:message key="page.city.kv"/></option>
            <option class="Belarus" value="Minsk"><fmt:message key="page.city.mn"/></option>
            <option class="Belarus" value="Grodno"><fmt:message key="page.city.gr"/></option>
            <option class="Belarus" value="Polotsk"><fmt:message key="page.city.pl"/></option>
        </select>
        <input type="submit" name="button" value=<fmt:message key="page.button.find"/>>
    </form>

    <c:forEach var="parlor" items="${list}">
        <form action="TattooClub" method="get">
            <input type="hidden" name="command" value="feedback">
        <table align="center" cellspacing="0">
            <tbody>
            <tr>
                <td></td>
                <br>
                    <h2>${parlor.name}</h2>
                    <c:out value="${parlor.country}"/>,
                    <c:out value="${parlor.city}"/>,
                    <c:out value="${parlor.address}"/><br>
            </tr>
            </tbody>
            <input type="hidden" name="idParlor" value="${parlor.id}">
            <input class="submit" type="submit" value=<fmt:message key="page.button.feedback"/>>
        </table>
        </form>
        <hr>
    </c:forEach>
</main>

<c:import url="/jsp/common/footer.jsp"/>
</body>
</html>
