<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>
<html>
<head>
    <title><fmt:message key="page.menu.singUp"/></title>
    <style>
        .alert{margin-top: 50px;}
        .form{margin-top: 80px;}
    </style>
</head>
<body>
<header>
    <c:import url="common/header.jsp"/>
</header>
<c:set var="path" scope="session" value="${pageContext.request.requestURI}"/>

<c:choose>
    <c:when test="${requestScope.message ne null}">
        <div class="alert alert-danger" align="center">
                ${message}
        </div>
    </c:when>
</c:choose>

<form action="/TattooClub" method="post" class="form">
    <input type="hidden" name="command" value="signUp"/>
    <table align="center">
        <tr>
            <td><label for="login"><fmt:message key="page.menu.username"/></label></td>
            <td><input type="text" name="name" id="login" pattern="[А-ЯA-Z][a-яa-z]{2,30}" minlength="1"
                       title="Name shouldn't contain numbers,symbols and should start with capital letter" required></td>
        </tr>
        <tr>
            <td><label for="email"><fmt:message key="page.menu.email"/></label></td>
            <td><input type="email" name="email" id="email"
                   pattern="[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)$"
                       title="Incorrect email"  required></td>
        </tr>
        <tr>
            <td><label for="password"><fmt:message key="page.menu.password"/></label></td>
            <td><input type="password" name="password" id="password" pattern="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}"
                   minlength="6" title="password should contain min 6 symbols,1 capital letter, 1 number" required></td><br>
        </tr>
        <tr>
            <td><label for="repPassword"><fmt:message key="page.menu.repeatPassword"/></label></td>
            <td><input type="password" name="confirmPassword" id="repPassword" pattern="(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z]{6,}"
                   minlength="6" title="password should contain min 6 symbols,1 capital letter, 1 number" required></td>
        </tr>
        <tr>
            <td><button type="submit"><fmt:message key="page.menu.register"/></button></td>
        </tr>
    </table>
</form>
<c:import url="/jsp/common/footer.jsp"/>
</body>
</html>
