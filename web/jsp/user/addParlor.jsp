<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<fmt:setLocale value="${locale}" scope="session"/>
<fmt:setBundle basename="content"/>
<html>
<head>
    <title>Parlor Registration</title>
    <link rel="stylesheet"  href="/css/addParlor.css"/>
    <script src="/js/parlor.js"></script>
    <style>main{position: relative;
        top:80px}</style>
</head>
<body>
<header>
    <c:import url="../common/header.jsp"/>
</header>
<c:set var="path" scope="session" value="${pageContext.request.requestURI}"  />

<main>
<c:choose>
    <c:when test = "${sessionScope.idParlor ne '0' and sessionScope.role ne 'admin'}" >
    <div><fmt:message key="page.parlor.haveParlor"/></div>
    </c:when>
    <c:otherwise>
    <h1 id="header"><fmt:message key="page.parlor.addParlor"/></h1>
    <form action="/TattooClub" method="post" >
        <input type="hidden" name="command" value="addParlor" />
        <div align="center">

            <select name="country" id="country" >
                <option value="select"><fmt:message key="page.country.choose"/></option>
                <option value="Belarus"><fmt:message key="page.country.by"/></option>
                <option value="Russia"><fmt:message key="page.country.ru"/></option>
                <option value="Ukraine"><fmt:message key="page.country.ukr"/></option>

            </select>
            <select name="city" id="city" >
                <option class="select" selected disabled value=''><fmt:message key="page.city.choose"/></option>
                <option class="Russia" value="Moscow"><fmt:message key="page.city.msc"/></option>
                <option class="Russia" value="Saint Petersburg"><fmt:message key="page.city.spb"/></option>
                <option class="Russia" value="Volgograd"><fmt:message key="page.city.vlg"/></option>
                <option class="Ukraine" value="Kiev"><fmt:message key="page.city.kv"/></option>
                <option class="Belarus" value="Minsk"><fmt:message key="page.city.mn"/></option>
                <option class="Belarus" value="Grodno"><fmt:message key="page.city.gr"/></option>
                <option class="Belarus" value="Polotsk"><fmt:message key="page.city.pl"/></option>
            </select>
            <table class="tab">
                <tr>
                    <td><fmt:message key="page.parlor.name"/></td><td><input type="text" name="parlorName" required></td>
                </tr>
                <tr>
                    <td><fmt:message key="page.parlor.address"/></td><td><input type="text" name="address" required></td>
                </tr>
                <tr>
                    <td><button type="submit" ><fmt:message key="page.button.submit"/></button> </td>
                </tr>
            </table>
        </div>
    </form>
    </c:otherwise>
</c:choose>


    <c:import url="/jsp/common/footer.jsp"/>
</body>
</html>
